<?php
error_reporting(E_ALL & ~(E_STRICT|E_NOTICE|E_DEPRECATED));
ini_set('display_errors',1);

require_once("core/BI.class.php");
require_once("vendor/autoload.php");
BI::init("app/");


//aby uruchomić framework bez uruchamiania routera
//BI::init("app/", false);
