<?php

interface IConfigLoader {
   
    public function setWrapper(Config $configObject);
   
    public function readConfig($fileName);
   
}
