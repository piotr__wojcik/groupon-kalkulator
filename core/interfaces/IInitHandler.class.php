<?php

interface IInitHandler {
   
   public function init();
   
   public function execute();
   
}
