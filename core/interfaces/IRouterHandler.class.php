<?php

interface IRouterHandler {
   
   public function translateRequest();
   
   public function beforeExecute();
   
   public function doExecute();
   
   public function afterExecute();
   
}
