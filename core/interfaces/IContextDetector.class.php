<?php

interface IContextDetector {
   
   public function detectContextAfterParsingRequest();
   
   public function detectContextAfterRouting();
   
}
