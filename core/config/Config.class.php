<?php
require_once('core/interfaces/IConfigLoader.class.php');

final class Config {
   
   //static properties (this is a singleton)
   
   static private $instance = NULL;
   
   //to init singleton
   static public function init() {
      if (self::$instance == NULL) {
         self::$instance = new Config();
      }
   }
   
   //getter for config object
   static public function get() {
      return self::$instance;
   }
   
   //set the loader of the config (handler is responsible for loading the config from a proper format)
   static public function setLoader(IConfigLoader $loader) {
      self::$instance->loader = $loader;
      $loader->setWrapper(self::$instance);
      self::$instance->loader->readConfig(BI::$configDirectory.'default');
   }

   //BI wants to check whether we have a loader
   static public function hasLoader() {
      return self::$instance->loader!==NULL;
   }
   
   //to read config files
   static public function read($filename) {
      self::$instance->loader->readConfig(BI::$configDirectory.$filename);
   }
   
   //object properties (this is a singleton)
   
   private $loader = NULL;
   private $configuration = array();
   
   //private constructor
   private function __construct() {
   
   }
   
   public function appendConfiguration($configuration) {
      if (!is_array($configuration)) {
         die("FAILED TO LOAD CONFIGURATION: Config->appendConfiguration() - configuration data is not an array");
      }
      $this->configuration = array_merge($this->configuration, $configuration);
   }
   
   public function &__get($name) {
      if (!isset($this->configuration[$name])) {
         die("Config->get() - no element with id: {$name}");
      }

      return $this->configuration[$name];
   }
}
