<?php



//pełne kasowanie katalogu
function fullrmdir($dir) {

   if (substr($dir,0,-1)!="/")
      $dir.="/";
   if (!is_dir($dir))
      return FALSE;

   if ($handle = opendir($dir)) {
      $dirsToVisit = array();
      while (false !== ($file = readdir($handle))) {
         if ($file != "." && $file != "..") {

            if (is_dir($dir.$file))
               $dirsToVisit[] = $dir.$file;

            else if (is_file($dir.$file)) {
               unlink($dir.$file);
            }
         }
      }
      closedir($handle);
      foreach ($dirsToVisit as $i=>$w)
         fullrmdir($w);
   }
   rmdir($dir);
   return true;
}

//pełne tworzenie katalogu
function fullmkdir($folder) {
   if (!is_dir($folder)) {
      $sciezka = explode("/", $folder);
      $pom = "";
      foreach ($sciezka as $i=>$w) {
         if ($w=="")
            continue;

         $pom.="{$w}/";
         if (!is_dir($pom))
            mkdir($pom);
      }
   }
}

//ogólne fetch assoc
function fetch_assoc($wynik) {
   return mysqli_fetch_assoc($wynik);
}

//ogólne num rows
function num_rows($wynik) {
   return mysqli_num_rows($wynik);
}

//ogólne escape string
function escape_string($text) {
   return BazaDanych::escapeString($text);
}

//ogólne link
function makeLink($href = "", $admin = false) {
   if (strlen($href)>0 && substr($href,0,1)=="/")
      $href = substr($href,1);

   return "/".($admin ? "admin/" : "").$href;
}

//absolute link
function makeAbsoluteLink($addr = "") {
    $url = (!empty($_SERVER['HTTPS']) ? "https://" : "http://").$_SERVER['SERVER_NAME'].makeLink($addr);
	return $url;
}

//czy poprawny adres email
function isEmail($email) {
   $email = trim($email);
   $dobryEmail = preg_match('/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/',trim($email));
   if (!$dobryEmail) {
      return false;
   }
   
   return true;
}

//robi float z danej (jeśli striny to zmieni na 0), a ogólnie chodzi o zamianę przecinka na kropkę
function makeFloat(&$float) {
   $search = array(",");
   $replace = array('.');
   $float = str_replace($search, $replace, $float);
   $float = (float)$float;
   return $float;
}

//zmienia na liczbę z określoną liczbą miejsc po przecinku
function makeDecimal(&$liczba, $lMiejscDziesietnych = 0) {
   makeFloat($liczba);
   $liczba = number_format($liczba, $lMiejscDziesietnych, '.', '');
   $copy = $liczba;
   return $copy;
}

//czysci dane za pomoca trim i striptags
function cleanData(&$data) {
   if (is_array($data)) {
      foreach ($data as $i=>$w)
         if (is_array($w))
            cleanData($data[$i]);
         else
            $data[$i] = trim(strip_tags($w));
   }
   else
      $data = trim(strip_tags($data));
}

function cleanSomeData(&$string, $additionalTags = "") {
   $string = trim(strip_tags($string, "{$additionalTags}"));
}

//czysci dane za pomoca trim
function trimData(&$data) {
   if (is_array($data)) {
      foreach ($data as $i=>$w)
         if (is_array($w))
            trimData($data[$i]);
         else
            $data[$i] = trim($w);
   }
   else
      $data = trim($data);
}

function escapeData(&$data) {
   if (is_array($data)) {
      foreach ($data as $i=>$w)
         if (is_array($w))
            escape_string($data[$i]);
         else
            $data[$i] = escape_string($w);
   }
   else
      $data = escape_string($data);
}

function stripslashesData(&$data) {
   if (is_array($data)) {
      foreach ($data as $i=>$w)
         if (is_array($w))
            stripslashes($data[$i]);
         else
            $data[$i] = stripslashes($w);
   }
   else
      $data = stripslashes($data);
}

//sprawdza czy na początku stringa jest http:// jeśli nie to dodaje
function properUrl(&$url) {
   $url = trim($url);
   //jeśli nie ma http:// na początku to dodajemy
   if ( (strncmp("http://", $url, 7)) !== 0)
      if (isUrl("http://".$url))
         $url = "http://".$url;
}

//sprawdza czy url
function isUrl($url) {
   return filter_var($url, FILTER_VALIDATE_URL) !== false;
}

//przycina string do długości length - jeśli dłuższy to dodaje kropki
function cutString($string, $length) {
   $len = strlen($string);
   return substr($string, 0, $length).($len>$length ? "..." : "");
}

function smartCutPlainText($string, $length) {
   $len = mb_strlen($string,"utf-8");
   $string = mb_substr($string, 0, $length,"utf-8");

   if ($length<$len) {
      for ($i = $length-1; $i>0; $i--) {
         $chr = mb_substr($string, $i, 1,"utf-8");
         if ($chr == " " || $chr == ".") {
            $string = mb_substr($string, 0, $i,"utf-8");
            break;
         }
      }
      $string.="...";
   }

   return $string;
}

//przycina string z tagami html do długości length tak aby domknąć wszystkie tagi
function smartCutString($string, $length) {
   $len = mb_strlen($string,"utf-8");
   $string = mb_substr($string, 0, $length,"utf-8");

   for ($i = $length-1; $i>0; $i--) {
      $chr = mb_substr($string, $i, 1,"utf-8");
      if ($chr == ">")
         break;
      else if ($chr == "<")
         $string = mb_substr($string, 0, $i,"utf-8");
   }
   if ($len>$length)
      $string.="...";

   return smartCutStringPom($string);
}

//pomocnicza dla powyższej
function smartCutStringPom($text) {
   //domykanie tagów
   $tags = array();

   $i = mb_strpos($text,'<');

   if ($i===FALSE)
      return $text;

   while ($i>=0 && $i<mb_strlen($text) && $i!==FALSE) {
      $j = mb_strpos($text,'>',$i);
      if ($j === FALSE)
         break;
      $k = mb_strpos($text,' ',$i);
      if ($k > $i && $k < $j) {
         $tag = mb_substr($text,$i+1,$k-$i-1);
      } else {
         $tag = mb_substr($text,$i+1,$j-$i-1);
      }
      $tag = strtolower($tag);

      if ( (mb_strpos($tag,'/')) === 0) {

         $tag = mb_substr($tag,1);

         if ($tags[count($tags)-1]==$tag) {
            unset($tags[count($tags)-1]);

         }
         else {
            //to nie powinno się zdarzyć :|
         }
      } else {
         $tags[count($tags)] = $tag;
      }

      $i = mb_strpos($text,'<',$j);

   }

   //i dodajemy
   if (count($tags)>0) {
      for ($i=count($tags)-1; $i>=0; $i--)
         $text.="</{$tags[$i]}>";
   }
   return $text;
}

//czyści nazwę pliku (można podać pełną ścieżkę do pliku) tak aby zawierała tylko [0-9] [a-z] [A-Z] oraz _-
function cleanFileName(&$filename) {
   $path = pathinfo($filename);
   $len = strlen($path['filename']);
   $ret = "";
   for ($i=0; $i<$len;$i++) {
      $chr = $path['filename'][$i];
      if ( (ord($chr)>=65 && ord($chr)<=90) || (ord($chr)>=97 && ord($chr)<=122) || (ord($chr)>=48 && ord($chr)<=57))
         $ret.=$chr;
      else if ($chr==" ")
         $ret.="_";
      else if ($chr=="-")
         $ret.=$chr;
      else if ($chr=="_")
         $ret.=$chr;
   }
   return $ret.".{$path['extension']}";
}

//przydatne dla js, aby string był w jednej linii (i np: dodać go do skryptu) - nie czyści apostrofów lub cudzysłowów!
function inline($content) {
	// Strip newline characters.
	$content = str_replace(chr(10), " ", $content);
	$content = str_replace(chr(13), " ", $content);
	// Replace single quotes.
	$content = str_replace(chr(145), chr(39), $content);
	$content = str_replace(chr(146), chr(39), $content);
	// Return the result.
	//return addslashes($content);
	return $content;
}

//robi ze stringa id tekstowe (zamienia polskie znaki na łacińskie i kasuje znaki specjalne inne niż _-
function textId($msg) {
   $msg = mb_strtolower($msg,'utf-8');
   $trans = array(
      "ń" => "n",
      "ę" => "e",
      "ó" => "o",
      "ą" => "a",
      "ś" => "s",
      "ł" => "l",
      "ż" => "z",
      "ź" => "z",
      "ć" => "c",
      );
   $msg = strtr($msg, $trans);
   $len = mb_strlen($msg, 'utf-8');
   $ret = "";
   $lastSign = "";
   for ($i=0; $i<$len;$i++) {
      $chr = mb_substr($msg,$i,1,'utf-8');
      if ( (ord($chr)>=65 && ord($chr)<=90) || (ord($chr)>=97 && ord($chr)<=122) || (ord($chr)>=48 && ord($chr)<=57)) {
         $ret.=$chr;
         $lastSign = $chr;
      }
      else if ($chr==" ") {
         if ($lastSign=="-") {
            continue;
         }
         $ret.="-";
         $lastSign = "-";
      }
      else if ($chr=="-") {
         if ($lastSign=="-") {
            continue;
         }
         $ret.=$chr;
         $lastSign = "-";
      }
      else if ($chr=="-") {
         if ($lastSign=="-") {
            continue;
         }
         $ret.=$chr;
         $lastSign = "-";
      }
   }
   return $ret;
}

//robi ze stringa id tekstowe (zamienia polskie znaki na łacińskie i znaki przestankowe zmienia na -
function userTextId($msg) {
   $msg = mb_strtolower($msg,'utf-8');
   $trans = array(
      "ń" => "n",
      "ę" => "e",
      "ó" => "o",
      "ą" => "a",
      "ś" => "s",
      "ł" => "l",
      "ż" => "z",
      "ź" => "z",
      "ć" => "c",
      );
   $msg = strtr($msg, $trans);
   $len = mb_strlen($msg, 'utf-8');
   $ret = "";
   for ($i=0; $i<$len;$i++) {
      $chr = mb_substr($msg,$i,1,'utf-8');
      if ( (ord($chr)>=65 && ord($chr)<=90) || (ord($chr)>=97 && ord($chr)<=122) || (ord($chr)>=48 && ord($chr)<=57))
         $ret.=$chr;
      else if ($chr==" ")
         $ret.="-";
      else if ($chr=="-")
         $ret.=$chr;
      else if ($chr=="_")
         $ret.="-";
   }
   return $ret;
}

function microtime_float() {
   list($usec, $sec) = explode(" ", microtime());
   return ((float)$usec + (float)$sec);
}

function trimToPossibleFields(&$dane, $possibleFields) {
   if ($possibleFields == NULL)
      return;

   //tablice zostawiam bez zmian, przycinam wszystko co tablicą nie jest
   foreach ($dane as $i=>$w) {
      if (is_array($w))
         continue;
      else if (!in_array($i, $possibleFields))
         unset($dane[$i]);
   }
}

function htmlSafe($msg) {
   return inline(htmlentities($msg, ENT_QUOTES | ENT_SUBSTITUTE, 'utf-8'));
}

function trace() {
   $trace = debug_backtrace();
   $msg = "<br /><br /><br />\n\n\nTrace: ";
   foreach ($trace as $i=>$dane) {
      $msg.="<br />\n=======================================================<br />\n";
      //var_dump($trace);
      foreach ($dane as $i2=>$w) {
         if ($i2!="object") {
            $msg.="{$i2}: " . (is_array($w) ? print_r($w,true) : $w) . "<br />\n";
         }
      }
   }
   trigger_error($msg,E_USER_WARNING);
}

function redirect($url) {
   $_SESSION['log'] = \Log::getSave();
   header("Location: {$url}");
   die();
}

//hashowanie

function pbkdf2($algorithm, $password, $salt, $count, $key_length, $raw_output = false) {
   $algorithm = strtolower($algorithm);
   if(!in_array($algorithm, hash_algos(), true))
     die('PBKDF2 ERROR: Invalid hash algorithm.');
   if($count <= 0 || $key_length <= 0)
     die('PBKDF2 ERROR: Invalid parameters.');
   
   $hash_length = strlen(hash($algorithm, "", true));
   $block_count = ceil($key_length / $hash_length);
   
   $output = "";
   for($i = 1; $i <= $block_count; $i++) {
     // $i encoded as 4 bytes, big endian.
     $last = $salt . pack("N", $i);
     // first iteration
     $last = $xorsum = hash_hmac($algorithm, $last, $password, true);
     // perform the other $count - 1 iterations
     for ($j = 1; $j < $count; $j++) {
         $xorsum ^= ($last = hash_hmac($algorithm, $last, $password, true));
     }
     $output .= $xorsum;
   }
   
   if($raw_output)
     return substr($output, 0, $key_length);
   else
     return bin2hex(substr($output, 0, $key_length));
}  
   
function _hash($string, $algorithm = "sha256", $count = 1024) {
   for ($j = 0; $j < $count; $j++) {
      $string = hash($algorithm, Config::get()->HASH_SALT.$string, true);
   }
   return bin2hex($string);
}  
