<?php

final class Context {
   
   //static properties (this is a singleton)
   
   static private $instance = NULL;
   
   //to init singleton
   static public function init() {
      if (self::$instance == NULL) {
         self::$instance = new Context();
      }
   }
   
   //getter for config object
   static public function get() {
      return self::$instance;
   }
   
   //object properties (this is a singleton)
   
   private $data = array();
   
   //private constructor
   private function __construct() {
   
   }
   
   public function __set($name, $value) {
      $this->data[$name] = $value;
   }
   
   public function &__get($name) {
      if (!isset($this->data[$name])) {
         $this->data[$name] = array();
      }

      return $this->data[$name];
   }
   
   public function __isset($name) {
      if (isset($this->data[$name]))
         return true;
      return false;
   }
   
   public function __unset($name) {
      unset($this->data[$name]);
   }
   
}
