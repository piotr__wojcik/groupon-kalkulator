<?php
//we need the core config class to init the framework (we dont' have any autoload yet)
require_once('core/config/Config.class.php');

final class BI {
   static public $appDir = NULL;
   static public $configDirectory = NULL;

   //initiating framework
   static public function init($appDir, $autoExecute = true) {
      
      //load common functions
      require_once('core/functions.php');
      
      //ensure that we have lastModified stamp
      if (!is_file('lastModified.php')) {
         die("FAILED TO INIT FRAMEWORK: /lastModified.php missing");
      }
      else {
         require_once('lastModified.php');
         if (!defined('LAST_MODIFIED')) {
            die("FAILED TO INIT FRAMEWORK: /lastModified.php has to have a LAST_MODIFIED definition");
         }
      }
      
      //ensure that we have the app directory
      if (!is_dir($appDir)) {
         die("FAILED TO INIT FRAMEWORK: {$appDir} directory missing");
      }   
      
      //register autoload
      require_once('core/autoload.php');
      if (file_exists('vendor/autoload.php')) {
        require_once 'vendor/autoload.php';
      }
      //ensure that the last character in appdir is /
      if (substr($appDir,-1,1)!="/") {
         $appDir = $appDir.'/';
      }
      
      self::$appDir = $appDir;
      
      //init the config class
      Config::init();
      
      //this is where the config file should be located
      $bootstrapFile = $appDir.'config/bootstrap.php';
      
      //this where proper config files should be located
      self::$configDirectory = $appDir.'config/configs/';
      
      //ensure that we have the bootstrap file
      if (!file_exists($bootstrapFile)) {
         die("FAILED TO INIT FRAMEWORK: {$bootstrapFile} file missing");
      }
      
      //load the bootstrap file
      require_once($bootstrapFile);
      
      //ensure that we have a config loader
      if (!Config::hasLoader()) {
         die("FAILED TO INIT FRAMEWORK: no config loader defined in bootstrap file");
      }
      
      //init the request class
      Request::init();
      
      //run the full initialization
      $initiatorName = Config::get()->INIT_HANDLER;
      if ($initiatorName=="" || !class_exists($initiatorName)) {
         die("FAILED TO INIT FRAMEWORK: {$initiatorName} initiation handler class not found");
      }
      $initiator = new $initiatorName();
      $initiator->init();
      
      if ($autoExecute) {
         $initiator->execute();
      }
      
   } 
   
}
