<?php

final class Session {
   
   //static properties (this is a singleton)
   
   static private $instance = NULL;
   
   //to init singleton, and session handler
   static public function init() {
      if (self::$instance == NULL) {
         self::$instance = new Session();
      }
      //check whether session handler exists
      $handlerName = Config::get()->SESSION_HANDLER;
      if ($handlerName=="" || !class_exists($handlerName)) {
         die("FAILED TO INIT APP: {$handlerName} context handler class not found");
      }
      //create context handler (we will use it later, so we'll need it as a property
      self::$instance->handler = new $handlerName();
      self::$instance->handler->init();
   }
   
   //getter for config object
   static public function get() {
      return self::$instance;
   }
   
   //permanently save session
   static public function save() {
      //simply destroy the object - it's destructor will handle the rest
      self::$instance = NULL;
   }
   
   //object properties (this is a singleton)
   
   private $data = array();
   private $handler = NULL;
   
   //private constructor
   private function __construct() {
   
   }
   
   public function __destruct() {
      $this->handler->save();
   }
   
   public function __set($name, $value) {
      if ($this->handler == NULL) {
         die("Session not initiated");
      }
      $this->handler->$name = $value;
   }
   
   public function &__get($name) {
      if ($this->handler == NULL) {
         die("Session not initiated");
      }
      
      //this is a fix to return a reference
      $ref = &$this->handler->$name;
      return $ref;
   }
   
   public function __isset($name) {
      if ($this->handler == NULL) {
         die("Session not initiated");
      }
      return isset($this->handler->$name);
   }
   
   public function __unset($name) {
      if ($this->handler == NULL) {
         die("Session not initiated");
      }
      unset($this->handler->$name);
   }
   
}
