<?php

class Log {
   static private $instance = NULL;
   
   //=================================================
   // funkcje klasowe
   
   static public function error($msg, $szufladka = NULL) {
      if (self::$instance == NULL) {    
         self::$instance = new Log();
      }
      self::$instance->dopiszError($msg, $szufladka);
   }
   
   static public function confirm($msg, $szufladka = NULL) {
      if (self::$instance == NULL) {    
         self::$instance = new Log();
      }
      self::$instance->dopiszConfirm($msg, $szufladka);
   }
   
   static public function warning($msg, $szufladka = NULL) {
      if (self::$instance == NULL) {    
         self::$instance = new Log();
      }
      self::$instance->dopiszWarning($msg, $szufladka);
   }
   
   static public function info($msg, $szufladka = NULL) {
      if (self::$instance == NULL) {    
         self::$instance = new Log();
      }
      
      self::$instance->dopiszInfo($msg, $szufladka);
   }
   
   static public function sysError($msg) {
      if (self::$instance == NULL) {    
         self::$instance = new Log();
      }
      self::$instance->dopiszSysError($msg);
   }
   
   static public function sysMsg($msg) {
      if (self::$instance == NULL) {    
         self::$instance = new Log();
      }
      self::$instance->dopiszSysMsg($msg);
   }
   
   static public function wypisz($szufladka = NULL) {
      if (self::$instance == NULL) {    
         self::$instance = new Log();
      }
      
      return self::$instance->wypiszLog($szufladka);
   }  
   
   static public function dlaFrontend($szufladka = NULL) {
      if (self::$instance == NULL) {    
         self::$instance = new Log();
      }
      if ($szufladka == NULL)
         $szufladka = 'globalsOnes';
      
      $data = array(
         'errors'=>self::$instance->errors[$szufladka],
         'warnings'=>self::$instance->warnings[$szufladka],
         'confirms'=>self::$instance->confirms[$szufladka],
         'infos'=>self::$instance->infos[$szufladka],
         );
      
      $obj = new View($data, 'main/global_message.php');
      return $obj->show();
   }  
   
   //=================================================
   // przechowywanie błędu podczas redirecta
   
   static public function getSave() {
      if (self::$instance != NULL) {    
         return serialize(self::$instance);
      }
      return serialize(NULL);
   }
   
   static public function readSave($save) {
      self::$instance = unserialize($save);
      
   }
   
   //=================================================
   // funkcje obiektu
   
   protected $errors = array('globalsOnes'=>array());
   protected $confirms = array('globalsOnes'=>array());
   protected $warnings = array('globalsOnes'=>array());
   protected $infos = array('globalsOnes'=>array());
   protected $sysErrors = array();
   protected $sysMsgs = array();
   
   private function __construct() {
      
   }

   private function dopiszError($msg, $szufladka) {
      if ($szufladka == NULL)
         $szufladka = 'globalsOnes';
         
      if (!isset($this->errors[$szufladka]))
            $this->errors[$szufladka] = array();
      
      if (!in_array($msg, $this->errors[$szufladka])) {
         $this->errors[$szufladka][] = $msg;
      }
   }
   
   private function dopiszConfirm($msg, $szufladka) {
      if ($szufladka == NULL)
         $szufladka = 'globalsOnes';
         
      if (!isset($this->confirms[$szufladka]))
            $this->confirms[$szufladka] = array();
               
      $this->confirms[$szufladka][] = $msg;
   }
   
   private function dopiszWarning($msg, $szufladka) {
      if ($szufladka == NULL)
         $szufladka = 'globalsOnes';
         
      if (!isset($this->warnings[$szufladka]))
            $this->warnings[$szufladka] = array();
               
      $this->warnings[$szufladka][] = $msg;
   }
   
   private function dopiszInfo($msg, $szufladka) {
      if ($szufladka == NULL)
         $szufladka = 'globalsOnes';
         
      if (!isset($this->infos[$szufladka]))
            $this->infos[$szufladka] = array();
               
      $this->infos[$szufladka][] = $msg;
   }
   
   private function wypiszLog($szufladka) {
      
      if ($szufladka == NULL)
         $szufladka = 'globalsOnes';
      $msg="";
      
      if (count($this->errors[$szufladka])>0) {
         $msg.='
			<div class="alert alert-error">
				<button type="button" class="close" data-dismiss="alert">&times;</button>';
         foreach ($this->errors[$szufladka] as $error)
            $msg.="{$error}<br>";
         $msg.="
         </div>
         ";
      }
      
      if (count($this->confirms[$szufladka])>0) {
         $msg.='
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>';
         foreach ($this->confirms[$szufladka] as $conf)
            $msg.="{$conf}<br>";
         $msg.="
         </div>
         ";
         
      }
      
      if (count($this->warnings[$szufladka])>0) {
         $msg.='
			<div class="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>';
         foreach ($this->warnings[$szufladka] as $conf)
            $msg.="{$conf}<br>";
         $msg.="
         </div>
         ";
      }
      
      if (count($this->infos[$szufladka])>0) {
         $msg.="
         <div class=\"infoDiv\">
         ";
         foreach ($this->infos[$szufladka] as $conf)
            $msg.="{$conf}<br />";
         $msg.="
         </div>
         ";
      }
      
      return $msg;
   }

   private function dopiszSysError($msg) {
      //debug($msg."<br>");
      $file = fopen('syslog.txt', 'a+');
      if ($file) {
         fwrite($file, print_r($msg, TRUE) . PHP_EOL);
         fclose($file);
      }
   }
   
   private function dopiszSysMsg($msg) {
   
   }
}
