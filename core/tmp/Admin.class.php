<?php

use app\admin\admin_user;

class Admin {
   private $loggedIn = false;
   private $uprawnienia = array();
   static private $instance = NULL;
   static private $sessionName = "Admin";
   static private $flaga = false;
   
   private function __construct() {
      
      if (isset($_SESSION[self::$sessionName]) && $_SESSION[self::$sessionName]!=NULL) {
         if (md5($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']) == $_SESSION[self::$sessionName]['source']) {
            $this->loggedIn = true;
            $this->uprawnienia = $_SESSION[self::$sessionName]['uprawnienia'];
         }
      }
      
      if (isset($_POST['Login'])) {
         $tmp = $this->sprawdzLogin($_POST['Login']['login'],_hash($_POST['Login']['haslo']));
         if (!$tmp)
            Log::error("Zły login lub hasło!");
         else {
            redirect("/admin");
         }
      }
   }
   
   private function sprawdzLogin($login, $haslo, $confirm = false) {
      $model = new admin_user();
      if ($model->login_with_password($login, $haslo)) {
         
         $this->logIn($model);
         
         return true;
      }
      
      return false;
   }
   
   static public function logInWithGoogle($auth_code)
   {
        $client = new Google_Client();
        $client->setClientId(\Config::get()->GOOGLE_OAUTH_CLIENT_ID);
        $client->setClientSecret(\Config::get()->GOOGLE_OAUTH_CLIENT_SECRET);
        $client->setRedirectUri(\Config::get()->GOOGLE_OAUTH_REDIRECT_URI);

        $client->addScope("email");
        $client->addScope("profile");      
                
        $service = new Google_Service_Oauth2($client);
        
        $client->authenticate($auth_code);
        $user = $service->userinfo->get();
        $email = $user->email;
        $model = new admin_user();
        
        if ($model->login_with_google($email)) {
            self::inicjalizuj();
            self::$instance->logIn($model);
            $_SESSION[self::$sessionName]['googleAccessToken'] = $client->getAccessToken();
            //usage $client->setAccessToken($_SESSION[self::$sessionName]['googleAccessToken']);
            return true;
        }
      
        return false;
   }
   
   public function logIn($model)
   {
         $this->loggedIn = true;
         $_SESSION[self::$sessionName] = array();
         $_SESSION[self::$sessionName]['name'] = $model->data['login'];
         $_SESSION[self::$sessionName]['id'] = $model->id;
         $_SESSION[self::$sessionName]['source'] = md5($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);
         $_SESSION[self::$sessionName]['uprawnienia'] = $model->data['privileges'];
         
   }
 
   private function zalogowanyUser() {
      return $this->loggedIn;
   }
   
   //sprawdzanie czy ktoś może edytować treść danej sekcji
   
   private function sprawdzUprawnienia($idUprawnienia) {
      if (!$this->zalogowanyUser())
         return false;
         
      if (!is_array($this->uprawnienia))
         return false;
      
      if (in_array('all', $this->uprawnienia)) {
         return true;
      }
      
      return in_array($idUprawnienia, $this->uprawnienia);
   }
   
   static private function inicjalizuj() {
      if (self::$instance == NULL) {
         self::$instance = new Admin();
      }
   }
   
   //=====================================================================
   // publiczne
   
   static public function logout() {
      $_SESSION[self::$sessionName] = NULL;
      unset($_SESSION[self::$sessionName]);
   }
   
   static public function zalogowany() {
      self::inicjalizuj();
      return self::$instance->zalogowanyUser();
   }

   static public function czyMoze($idUprawnienia) {
      self::inicjalizuj();
      
      return self::$instance->sprawdzUprawnienia($idUprawnienia);
   }
    
   static public function dajLogin() {
      return $_SESSION[self::$sessionName]['name'];
   }  
   
   static public function dajId() {
      return $_SESSION[self::$sessionName]['id'];
   }  
   
   static public function dajModel()
   {
       if (!self::zalogowany()) {
           return false;
       }
       $model = new admin_user($_SESSION[self::$sessionName]['id']);
       $model->getData(true);
       return $model;
   }
   
    static $minions = null;
   
    static public function getMinions()
    {
        if (!self::zalogowany()) {
            return array();
        }
        if (self::$minions!=null) {
            return self::$minions;
        }
        
        $m = self::dajModel();
        self::$minions = $m->getMinions();
        
        return self::$minions;
    }
}

