<?php

class imageUploader {
   
   const RESIZE = "resize";
   const CUT = "cut";

   static public function checkFile($zmienna, $width = 0, $height = 0) {
      if ($_FILES[$zmienna]['tmp_name']=="") {
         return 1; //brak pliku
      }

      $plik = $_FILES[$zmienna]['tmp_name'];
      $info = getimagesize($plik);

      if ($info===FALSE)
         return 2; //najprawdopodobnije nie jest to plik

      $allowedTypes = array('image/gif','image/jpeg','image/png');
      if (!in_array($info['mime'], $allowedTypes))
         return 3; //zły typ obrazka

      if ($width!=0 && $height!=0) {
         //sprawdzamy rozmiar
         if ($info[0]<$width)
            return 4; //za mała szerokość
         if ($info[1]<$height)
            return 4; //za mała wysokość
      }

      if ($width!=0 || $height!=0) {
         //sprawdzamy rozmiar
         if ($width!=0 && $info[0]<$width)
            return 4; //za mała szerokość
         if ($height!=0 && $info[1]<$height)
            return 4; //za mała wysokość
      }

      return 0;
   }

   static public function readFile($file) {
      $fileInfo = GetImageSize($file);
      if ($fileInfo['mime'] == "image/gif")
         $zrodlo=ImageCreateFromGIF($file);
      else if ($fileInfo['mime'] == "image/jpeg")
         $zrodlo=ImageCreateFromJPEG($file);
      else if ($fileInfo['mime'] == "image/png")
         $zrodlo=ImageCreateFromPNG($file);

      return $zrodlo;
   }

   static public function saveProcessedFile($obrazek, $docelowy, $fileInfo) {
      if ($fileInfo['mime'] == "image/gif")
         ImageGIF($obrazek, $docelowy);
      else if ($fileInfo['mime'] == "image/jpeg")
         ImageJPEG($obrazek, $docelowy, 100);
      else if ($fileInfo['mime'] == "image/png")
         ImagePNG($obrazek, $docelowy, 0);
   }
   
   static public function prepareFileName($name, $folder) {
      $nowaNazwaPliku = cleanFileName($name);

      fullmkdir($folder);

      while (file_exists($folder . $nowaNazwaPliku)) {
         $nowaNazwaPliku = '_' . $nowaNazwaPliku;
      }

      return $nowaNazwaPliku;
   }
   
   static public function saveFile($zmienna, $docelowy, $folder, $operacja, $width, $height) {
      
      $fileName = self::prepareFileName($docelowy, $folder);
      $fileInfo = GetImageSize($_FILES[$zmienna]['tmp_name']);
      $zrodlo = self::readFile($_FILES[$zmienna]['tmp_name']);
      if ($operacja == self::CUT) {
         if ($fileInfo[0]<$width || $fileInfo[1]<$height) {
            $newFile = self::resizeFile($zrodlo, $width, $height, $fileInfo);
         } else {
            $newFile = self::cutFile($zrodlo, $width, $height, $fileInfo);
         }
      } elseif ($operacja == self::RESIZE) {
         $newFile = self::resizeFile($zrodlo, $width, $height, $fileInfo);
      }
      
      self::saveProcessedFile($newFile, $folder.$fileName, $fileInfo);
      
      return $fileName;
   }

   static public function cutFile($zrodlo, $width, $height, $fileInfo) {
      $szer = $fileInfo[0];
      $wys = $fileInfo[1];



      if ($szer==$width && $wys==$height) {
         if ($fileInfo['mime'] == "image/gif" || $fileInfo['mime'] == "image/png") {
            imagealphablending($zrodlo, false);
         }
         if ($fileInfo['mime'] == "image/png") {
            imagesavealpha($zrodlo, true);
         }
         return $zrodlo;
      }

      $stosunek = $width/$height;
      $stosunekOryginalny = $szer/$wys;

      if ($stosunekOryginalny>$stosunek) {
         //$size2 to rozmiary pośredniego obrazka, do którego wycinamy z oryginalnego
         $size2h = $wys;
         $size2w = $wys*$stosunek;
         //wspołrzędnie gdzie zaczniemy wycinanie
         $xstart = ($szer - $size2w)/2;
         $ystart = 0;
      }
      //w p.p. wiemy że tniemy na wysokości
      else {
         //$size2 to rozmiary pośredniego obrazka, do którego wycinamy z oryginalnego
         $size2h = $szer*(1/$stosunek);
         $size2w = $szer;
         //wspołrzędnie gdzie zaczniemy wycinanie
         $xstart = 0;
         $ystart = ($wys - $size2h)/2;
      }

      $obrazek=imagecreatetruecolor($width,$height);

      if ($fileInfo['mime'] == "image/gif" || $fileInfo['mime'] == "image/png") {
         imagealphablending($obrazek, false);
         imagealphablending($zrodlo, false);
      }
      if ($fileInfo['mime'] == "image/gif") {
         # get and reallocate transparency-color
       	$transindex = imagecolortransparent($zrodlo);
       	if($transindex >= 0) {
       		$transcol = imagecolorsforindex($zrodlo, $transindex);
       		$transindex = imagecolorallocatealpha($obrazek, $transcol['red'], $transcol['green'], $transcol['blue'], 127);
       		imagefill($obrazek, 0, 0, $transindex);
       	}
      }

      ImageCopyResampled($obrazek, $zrodlo, 0, 0 , $xstart, $ystart, $width, $height, $size2w, $size2h);

      if ($fileInfo['mime'] == "image/png") {
         imagesavealpha($obrazek, true);
         imagesavealpha($zrodlo, true);
      }
      if ($fileInfo['mime'] == "image/gif") {
         if($transindex >= 0) {
       		imagecolortransparent($obrazek, $transindex);
       		for($y=0; $y<$h; ++$y)
               for($x=0; $x<$w; ++$x)
                  if(((imagecolorat($obrazek, $x, $y)>>24) & 0x7F) >= 100) imagesetpixel($obrazek, $x, $y, $transindex);

       	}
         imagetruecolortopalette($obrazek, true, 255);
 	      imagesavealpha($obrazek, false);
      }

      return $obrazek;
   }

   static public function resizeFile($zrodlo, $w, $h, $fileInfo) {
      $szer = $fileInfo[0];
      $wys = $fileInfo[1];

      if ($szer<$w && $wys<$h) {
         if ($fileInfo['mime'] == "image/gif" || $fileInfo['mime'] == "image/png") {
            imagealphablending($zrodlo, false);
         }
         if ($fileInfo['mime'] == "image/png") {
            imagesavealpha($zrodlo, true);
         }
         return $zrodlo;
      }

      $ratio1 = $w/$h;
      $ratio2 = $szer/$wys;

      //jesli wysokosc obrazka większa niz limit ale szerokosc mniejsza niz limit
      // to dostosowujemy szerokosc limitu wg proporcji
      if ($wys>$h && $szer<$w) {
         $w = round($h*$ratio2);
      }
      //jesli szerokosc obrazka większa niz limit ale wysokosc mniejsza niz limit
      // to dostosowujemy wysokosc limitu wg proporcji
      else if ($wys<$h && $szer>$w) {
         $h = round($w*(1/$ratio2));
      }
      //jeśli zarówno wysokość jak i szerokość są większe niż limit, to wybieramy
      //odp. wymiar w zależności od proporcji oryginalnej wzgl. zadanej
      else {
         //zadana proporcja jest większa zatem poprawiamy szerokosc
         if ($ratio1>$ratio2) {
            $w = round($h*$ratio2);
         }
         //zadan proporcja jest mniejsza, ergo wysokość poprawiamy
         else {
            $h = round($w*(1/$ratio2));
         }
      }
      $obrazek=imagecreatetruecolor($w,$h);

      if ($fileInfo['mime'] == "image/gif" || $fileInfo['mime'] == "image/png") {

         imagealphablending($zrodlo, false);
         imagealphablending($obrazek, false);
      }
      if ($fileInfo['mime'] == "image/gif") {
         # get and reallocate transparency-color
       	$transindex = imagecolortransparent($zrodlo);
       	if($transindex >= 0) {
       		$transcol = imagecolorsforindex($zrodlo, $transindex);
       		$transindex = imagecolorallocatealpha($obrazek, $transcol['red'], $transcol['green'], $transcol['blue'], 127);
       		imagefill($obrazek, 0, 0, $transindex);
       	}
      }

      ImageCopyResampled($obrazek, $zrodlo, 0, 0 , 0 , 0 , $w, $h, $szer, $wys);

      if ($fileInfo['mime'] == "image/png") {
         imagesavealpha($zrodlo, true);
         imagesavealpha($obrazek, true);
      }
      if ($fileInfo['mime'] == "image/gif") {
         if($transindex >= 0) {
       		imagecolortransparent($obrazek, $transindex);
       		for($y=0; $y<$h; ++$y)
               for($x=0; $x<$w; ++$x)
                  if(((imagecolorat($obrazek, $x, $y)>>24) & 0x7F) >= 100) imagesetpixel($obrazek, $x, $y, $transindex);

       	}
         imagetruecolortopalette($obrazek, true, 255);
 	      imagesavealpha($obrazek, false);
      }

      return $obrazek;
   }

   static public function zrobProporcjonalne($zrodlo, $fileInfo, $w, $h) {
      $szer = $fileInfo[0];
      $wys = $fileInfo[1];
      $width = $w;
      $height = $h;
      $stosunek = $width/$height;
      $stosunekOryginalny = $szer/$wys;

      //jesli obydwa wymiary oryginału są mniejsze niż limit, to od razu wiemy
      //jakie rozmiary bedzie mial oryginal w docelowym obrazku
      if ($szer<$width && $wys<$height) {
         //rozmiary oryginalu w docelowym obrazku
         $size2height = $wys;
         $size2width = $szer;
      }
      //w p.p. jesli któryś wymiar jest mniejszy niż limit, to ponieważ nie powiększamy
      //robimy to jako osobny przypadek
      else if ($szer<$width || $wys<$height) {
         //jesli proporcje oryginału są większe niż limitu (zazwyczaj 4:3, czyli oryginał dużo szerszy)
         if ($stosunekOryginalny>$stosunek) {
            //jesli to szerokosc oryginalna byla mniejsza niż w szerokosc limitu
            //to szerokosc zostawiamy
            if ($szer<$width) {
               $size2width = $szer;
            }
            //jesli to wysokosc byla mniejsza to narzucamy maks szerokosc oryginalu w docelowym
            else {
               $size2width = $width;
            }
            //dostosowujemy wysokosc wg oryginalej proporcji
            //wiemy, że nie przeskoczy limitu bo proporcja oryginału większa niż docelowego
             $size2height = round($size2width*(1/$stosunekOryginalny));
         }
         //jesli proporcje oryginału są mniejsze niż limitu (zazwyczaj 4:3, czyli oryginał dużo wyższy)
         else {
            //jesli to szerokosc oryginalna byla mniejsza niż w szerokosc limitu
            //to wysokosc ustawiamy na maksymalną i wyliczamy szerokosc (nie przeskoczy limity przez proporcje)
            if ($szer<$width) {
               $size2height = $height;
            }
            //jesli wysokosc oryginalna byla mniejsza niz limitu
            //(w zasadzie martwy przypadek, bo wtedy obydwa wymiary są mniejsze niż limit i wpadnie wcześniej w inny blok)
            else {
               $size2height = $wys;
            }
            //dostosowujemy wysokosc wg oryginalej proporcji
            //wiemy, że nie przeskoczy limitu bo proporcja oryginału mniejsza niż docelowego
            $size2width = round($size2height*$stosunekOryginalny);
         }
      }
      //wiemy, że obydwa wymiary oryginału większe niż limit
      //jesli stosunek oryginału większe niż limitu (zazwyczaj 4:3, czyli oryginał dużo szerszy)
      //to szerokosc oryginalu w docelowym ustawiamy na maksymalna w limicie
      // a wysokosc wyliczamy wg propocji
      else if ($stosunekOryginalny>$stosunek) {
         $size2width = $width;
         $size2height = round($size2width*(1/$stosunekOryginalny));
      }
      // w p.p.
      else {
         $size2height = $height;
         $size2width = round($size2height*$stosunekOryginalny);
      }

      //teraz współrzędne gdzie wkleimy oryginał do docelowego
      $xstart = ($width-$size2width)/2;
      $ystart = ($height-$size2height)/2;

      //tworzymy pośredni obrazek (przeskalowany oryginał z zachowaniem jego proporcji)
      $tmp=imagecreatetruecolor($size2width,$size2height) or die("Cannot Initialize new GD image stream");;
      ImageCopyResampled($tmp, $zrodlo, 0, 0 , 0 , 0 , $size2width, $size2height, $szer, $wys);

      //tworzymy docelowy i wypełniamy go białym kolorem
      $obrazek=imagecreatetruecolor($width,$height);
      $white = imagecolorallocate($obrazek, 255, 255, 255);
      imagefill($obrazek, 0, 0, $white);

      //kopiujemy z posredniego zaczynajac od odp. punktu
      ImageCopy($obrazek, $tmp, $xstart, $ystart, 0, 0,$size2width,$size2height);

      return $obrazek;
   }
   /*
   static public function proporcjonalne($plik, $width, $height) {
      $fileInfo = GetImageSize($plik);
      $zrodlo = self::readFile($plik);


      self::zrobProporcjonalne($zrodlo, $fileInfo, $width, $height, $plik);


      return $fileName;
   }
   */
}
