<?php

abstract class Model extends Component {
   
   public function __construct($id=NULL) {
      $this->id = $id;
      $this->baza = BazaDanych::dajBaze();
   }
   
   //===========================================================================
   // funkcje do obsługi elementList
   
   /*
   public function setParent($id) {
   
   }
   
   public function isParentNode() {
      return false;
   }
   */
   //===========================================================================
   //READ
   
   protected function sql() {
      return "select * from {$this->tabela} where id='{$this->id}'";
   }
   
   protected function doRead($data = NULL) {
      
      if ($data==NULL) {
         $data = fetch_assoc($this->baza->sql($this->sql()));
      }
      $this->rawData = array();
      if (is_array($data)) {
         foreach ($data as $id=>$w) {
            if (json_decode($w)!==null) {
               $this->data[$id] = $w;
            }
            else {
               $this->data[$id] = stripslashes($w);
            }
            $this->rawData[$id] = $w;
         }
      }
   }
   
   //===========================================================================
   //UPDATE
      
   protected function updateSql($string) {
      return "update " . $this->tabela . " set $string where id='{$this->id}'";
   }
   
   public function doUpdate($data) {
      if (isset($data['_counter'])) {
         unset($data['_counter']);
      }
      if (count($data)==0)
         return;
         
      $polecenie="";   
      foreach ($data as $id=>$w) {
         //tablice pomijam
         if (!is_array($w)) {
            $polecenie.="`$id`='" . escape_string($w) . "',";
         }
      }
      if ($polecenie!="") {
         $polecenie = substr($polecenie,0,-1);
         $this->baza->sql($this->updateSql($polecenie));
      }
   }
   
   //===========================================================================
   //ADD
    
   public function doAdd($data) {
      if (count($data)==0)
         return;
         
      $polecenie1="insert into {$this->tabela} (";
      $polecenie2=" values (";      
      foreach ($data as $id=>$w) {
         if (!is_array($w)) {
            $polecenie1.="`$id`,";
            $polecenie2.="'" . escape_string($w) . "',";
         }
      }
      $polecenie1 = substr($polecenie1,0,-1).")";
      $polecenie2 = substr($polecenie2,0,-1).")";
   
      $this->baza->sql($polecenie1.$polecenie2);
   }
   
   //===========================================================================
   //DELETE
   
   public function doDelete() {
      $this->baza->sql("delete from {$this->tabela} where id='{$this->id}'");
   }
    
   protected function getNewId() {
      $row = fetch_assoc($this->baza->sql("select max(id) as maks from {$this->tabela}"));
      return ($row['maks']+1);
   }
   
   protected function nowaKolejnosc($parent) {
      $row = fetch_assoc($this->baza->sql("select max(kolejnosc) as kol from {$this->tabela} where parent='{$parent}'"));
      return ($row['kol']+1);
   }
   
   protected function nowaKolejnosc2($where) {
      $row = fetch_assoc($this->baza->sql("select max(kolejnosc) as kol from {$this->tabela} where {$where}"));
      return ($row['kol']+1);
   }
}
