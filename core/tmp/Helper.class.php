<?php
class Helper {

   public function napisNadPolem($msg, $style = NULL) {
      return "<span class=\"napisNadPolem\" {$style}>$msg<br></span>";
   }

   public function nazwaPola($msg, $width = NULL, $style = NULL) {
      return "<span class=\"nazwaPola\" " . ($width!=NULL ? "style=\"width:{$width}px;{$style}\"" : "") . ">$msg</span>";
   }

   public function makeScriptHeader($sciezka) {
      return "<script type=\"text/javascript\" language=\"javascript\" src=\"{$sciezka}\"></script>";
   }

   public function makeCSSHeader($sciezka) {
      return "<link rel=\"stylesheet\" href=\"{$sciezka}\" type=\"text/css\">";
   }

   public function link($href, $admin = false) {
      return BASEPATH.($admin ? "/admin" : "").$href;
   }

   public function zakodujLink($link) {
      $trans = array(
      "/"=>"|",
      ":"=>";"
      );
      return strtr($link, $trans);
   }

   public function dekodujLink($link) {
      $trans = array(
      "|"=>"/",
      ";"=>":"
      );
      return strtr($link, $trans);
   }

   public function isEmail($email) {
      return isEmail($email);
   }

   public function isNumber($val) {
      return preg_match('/^[0-9]+((\.|,)[0-9]+)?$/',trim($val))>0;
   }

   public function isInt($val) {
      return preg_match('/^[0-9]+$/',trim($val))>0;
   }

   public function makeList(&$msg) {
      $pom = explode("\n",$msg);
      trimData($pom);
      $msg="";
      foreach ($pom as $i=>$w) {
         $pom[$i] = strip_tags($w,'<b><i><u><a>');
         self::repairHTML($pom[$i]);
         $czyPusty = trim(strip_tags($w),chr(194).chr(160))=="";

         if (!$czyPusty) {
            $msg.="<li>{$pom[$i]}</li>\n";
         }
      }

   }

   public function repairHTML(&$msg) {
      $text = $msg;
      //czyszczenie pustych p, li
      $text = preg_replace('/(^(<p>\s*(\s*&nbsp;\s*|\s*<br \/>\s*)*?\s*<\/p>\s*)*)|(^(<p>\s*(\s*&nbsp;\s*|\s*<br \/>\s*)*?\s*<\/p>\s*)*)|((<p>\s*(\s*&nbsp;\s*|\s*<br \/>\s*)*?\s*<\/p>\s*)*$)|((<li>\s*(\s*&nbsp;\s*|\s*<br \/>\s*)*?\s*<\/li>\s*)*$)/', '', $text);

      $text = preg_replace('/<h1>/', '<h1 class="cufon">', $text);
      $text = preg_replace('/<h2>/', '<h2 class="cufon">', $text);

      //domykanie tagów
      $tags = array();

      $i = mb_strpos($text,'<');

      while ($i>=0 && $i<mb_strlen($text) && $i!==FALSE) {
         $j = mb_strpos($text,'>',$i);
         if ($j === FALSE)
            break;
         $k = mb_strpos($text,' ',$i);
         if ($k > $i && $k < $j) {
            $tag = mb_substr($text,$i+1,$k-$i-1);
         } else {
            $tag = mb_substr($text,$i+1,$j-$i-1);
         }
         $tag = strtolower($tag);

         if ($tag!="br") {

            if ( (mb_strpos($tag,'/')) === 0) {

               $tag = mb_substr($tag,1);

               if ($tags[count($tags)-1]==$tag) {
                  unset($tags[count($tags)-1]);

               }
               else {
                  //to nie powinno się zdarzyć :|
               }
            } else {
               $tags[count($tags)] = $tag;
            }
         }

         $i = mb_strpos($text,'<',$j);

      }

      //i dodajemy
      if (count($tags)>0) {
         for ($i=count($tags)-1; $i>=0; $i--)
            $text.="</{$tags[$i]}>";
      }
      //i wynik
      $msg = $text;
   }

   public function makeFloat(&$float) {
      $search = array(",");
      $replace = array('.');
      $float = str_replace($search, $replace, $float);
      $float = (float)$float;
   }

   public function makeDecimal(&$liczba, $lMiejscDziesietnych = 0) {
      self::makeFloat($liczba);
      $liczba = number_format($liczba, $lMiejscDziesietnych, '.', '');
   }

   public function cleanData(&$data) {
      if (is_array($data))
         foreach ($data as $i=>$w)
            if (is_array($w))
               self::cleanData($data[$i]);
            else
               $data[$i] = trim(strip_tags($w));
   }

   public function cleanString(&$string) {
      $string = trim(strip_tags($string));
   }

   public function trimData(&$data) {
      if (is_array($data))
         foreach ($data as $i=>$w)
            if (is_array($w))
               self::trimData($data[$i]);
            else
               $data[$i] = trim($w," \t\n\r\0\x0B".chr(194).chr(160));
   }

   public function stripSomeTags(&$string) {
      $string = strip_tags($string, "<b><i><u><br><br/>");
   }

   public function validDateTime(&$string) {
      $pom = explode(" ",$string);
      $data = explode("-",$pom[0]);
      $czas = explode(":",$pom[1]);

      if (!checkdate($data[1],$data[2],$data[0])) {
      	return false;
      }
      if (count($czas)==0 && trim($pom[1])!="")
         return false;

      if ($czas[0]!="0" && $czas[0]!="00" && ($czas[0]<0 || $czas[0]>23))
         return false;

      if ($czas[1]!="0" && $czas[1]!="00" && ($czas[1]<0 || $czas[1]>59))
         return false;

      if (isset($czas[2]) && $czas[2]!="0" && $czas[2]!="00" && ($czas[2]<0 || $czas[2]>59))
         return false;

      $string = "{$pom[0]} " . (int)$czas[0] . ":" . (int)$czas[1] . ":" . (int)$czas[2];

      return true;
   }

   public function properUrl(&$url) {
      $url = trim($url);
      while (substr($url,0,1)=="/") {
         $url = substr($url,1);
      }
      //jeśli nie ma http:// na początku to dodajemy
      if ( (strncmp("http://", $url, 7)) !== 0)
         if (self::isUrl("http://".$url))
            $url = "http://".$url;
   }

   public function isUrl($url) {
      //z jakiegoś powodu ten serwer nie akceptuje myślinka i podkreślenia w url
      //$url = preg_replace('/-/','/a/',$url);
      //$url = preg_replace('/_/','/a/',$url);
      $urlregex = "^(https?|ftp)\:\/\/[a-z0-9+\$_-]+(\.[a-z0-9+\$_-]+)+(\/([a-z0-9+\$_-]\.?)+)*\/?(\?[a-z+&\$_.-][a-z0-9;:@/&%=+\$_.-]*)?(#[a-z_.-][a-z0-9+\$_.-]*)?\$";
      if (eregi($urlregex, $url))
         return true;
      else
         return false;
      //return filter_var($url, FILTER_VALIDATE_URL) !== false;
   }

   public function cutString($string, $length) {
      $len = strlen($string);
      return mb_substr($string, 0, $length, 'utf-8').($len>$length ? "..." : "");
   }

   public function smartCutString($string, $length) {
      $len = mb_strlen($string,"utf-8");
      $string = mb_substr($string, 0, $length,"utf-8");

      for ($i = $length-1; $i>0; $i--) {
         $chr = mb_substr($string, $i, 1,"utf-8");
         if ($chr == ">")
            break;
         else if ($chr == "<")
            $string = mb_substr($string, 0, $i,"utf-8");
      }
      if ($len>$length)
         $string.="...";

      return self::smartCutStringPom($string);
   }

   public function smartCutStringPom($text) {
      //domykanie tagów
      $tags = array();

      $i = mb_strpos($text,'<');

      if ($i===FALSE)
         return $text;

      while ($i>=0 && $i<mb_strlen($text) && $i!==FALSE) {
         $j = mb_strpos($text,'>',$i);
         if ($j === FALSE)
            break;
         $k = mb_strpos($text,' ',$i);
         if ($k > $i && $k < $j) {
            $tag = mb_substr($text,$i+1,$k-$i-1);
         } else {
            $tag = mb_substr($text,$i+1,$j-$i-1);
         }
         $tag = strtolower($tag);

         if ( (mb_strpos($tag,'/')) === 0) {

            $tag = mb_substr($tag,1);

            if ($tags[count($tags)-1]==$tag) {
               unset($tags[count($tags)-1]);

            }
            else {
               //to nie powinno się zdarzyć :|
            }
         } else {
            $tags[count($tags)] = $tag;
         }

         $i = mb_strpos($text,'<',$j);

      }

      //i dodajemy
      if (count($tags)>0) {
         for ($i=count($tags)-1; $i>=0; $i--)
            $text.="</{$tags[$i]}>";
      }
      return $text;
   }

   public function czasTrwania($seconds) {
      $minutes = floor($seconds/60);
      $secondsleft = $seconds % 60;

      if($secondsleft<10)
          $secondsleft = "0" . $secondsleft;
      return "$minutes:$secondsleft";
   }

   public function cleanFileName(&$filename) {
      $path = pathinfo($filename);
      $len = strlen($path['filename']);
      $ret = "";
      for ($i=0; $i<$len;$i++) {
         $chr = $path['filename'][$i];
         if ( (ord($chr)>=65 && ord($chr)<=90) || (ord($chr)>=97 && ord($chr)<=122) || (ord($chr)>=48 && ord($chr)<=57))
            $ret.=$chr;
         else if ($chr==" ")
            $ret.="_";
         else if ($chr=="-")
            $ret.=$chr;
         else if ($chr=="_")
            $ret.=$chr;
      }
      return $ret.".{$path['extension']}";
   }

   public function extensionFromImageMimetype($mimetype) {
      if ($mimetype == 'image/jpeg')
         return 'jpg';
      elseif ($mimetype == 'image/png')
         return 'png';
      elseif ($mimetype == 'image/gif')
         return 'gif';
      else {
         return 'jpg';
      }
   }

   public function isExternal($filename) {
      if (strncmp($filename,"http://",7)==0) {
         return true;
      }
      return false;
   }

   static public function zrob2Kolumny($tresc) {

      $ileZnakow = mb_strlen($tresc,'utf-8');
      $pom = explode("</p>",$tresc);

      $left = "";
      $right = "";

      if (count($pom)==0)
         return array($left,$right);

      $licznikZnakow = 0;
      $doklejanieDo = "left";
      foreach ($pom as $i=>$w) {
         if ($licznikZnakow>$ileZnakow / 2)
            $doklejanieDo = "right";

         if ($doklejanieDo=="left")
            $left.=$w."</p>";
         else
            $right.=$w."</p>";

         $licznikZnakow+= mb_strlen($w."</p>",'utf-8');
      }

      return array($left,$right);
   }

   //=================================================
   // wyspisywanie w linii (przydatne jeśli chodzi o js)

   public function inline($content) {
   	// Strip newline characters.
   	$content = str_replace(chr(10), " ", $content);
   	$content = str_replace(chr(13), " ", $content);
   	// Replace single quotes.
   	$content = str_replace(chr(145), chr(39), $content);
   	$content = str_replace(chr(146), chr(39), $content);
   	// Return the result.
   	//return addslashes($content);
   	return $content;
   }

   //=================================================
   // PARSERY DAT

   public function parserDaty($data) {
      //$miesiace = array("stycznia","lutego","marca","kwietnia","maja","czerwca","lipca","sierpnia","września","października","listopada","grudnia");
      $data = explode("-",$data);
      if (substr($data[2],0,1) == "0")
         $data[2] = substr($data[2],1);
      return $data[2].".{$data[1]}.".substr($data[0],2,2);
   }

   public function parserDaty2($data) {
      $miesiace = array("sty","lut","mar","kwi","maj","cze","lip","sie","wrz","paź","lis","gru");
      $data = explode("-",$data);
      if (substr($data[2],0,1) == "0")
         $data[2] = substr($data[2],1);
      return $data[2]." ".$miesiace[(int)$data[1]-1]." ".$data[0];
   }

   public function parserDaty3($data) {
      $miesiace = array("sty","lut","mar","kwi","maj","cze","lip","sie","wrz","paź","lis","gru");
      $pom = explode(" ",$data);
      $data = explode("-",$pom[0]);
      if (substr($data[2],0,1) == "0")
         $data[2] = substr($data[2],1);
      return $data[2]." ".$miesiace[(int)$data[1]-1]." ".$data[0].", {$pom[1]}";
   }

   public function parserDaty4($data) {
      $data = explode("-",$data);
      return "{$data[2]}.{$data[1]}.{$data[0]}";
   }
   
   public function parserDaty5($data) {
      $miesiace = array("stycznia","lutego","marca","kwietnia","maja","czerwca","lipca","sierpnia","września","października","listopada","grudnia");
      $pom = explode(" ",$data);
      $data = explode("-",$pom[0]);
      if (substr($data[2],0,1) == "0")
         $data[2] = substr($data[2],1);
      return $data[2]." ".$miesiace[(int)$data[1]-1]." ".$data[0];
   }

   public function tylkoData($data) {
      $pom = explode(" ",$data);
      if (count($pom)>1)
         return $pom[0];
      else
         return $data;

   }

   public function miesiacIRok($data) {
      if (Jezyki::aktualnyId()==1)
         $miesiace = array('styczeń','luty','marzec','kwiecień','maj','czerwiec','lipiec','sierpień','wrzesień','październik','listopad','grudzień');
      else
         $miesiace = array('january','february','march','april','may','june','july','august','september','october','november','december');

      $data = explode("-",$data);

      return array($miesiace[(int)$data[1]-1]." ".$data[0], "{$data[1]}-{$data[0]}");
   }

   //=================================================
   // ZNACZNIKI STRON

   public function znacznikiStronZwykle($strona, $ileStron, $adres) {
      $zakres = 4;
      $msg="
      <div class=\"znacznikiStronZwykle\">
      ";

      if ($strona + $zakres < $ileStron)
         $msg .= "<a class=\"tag\" href=\"{$adres}/pageNr-{$ileStron}\">{$ileStron}</a> ";

      if ($strona + $zakres < $ileStron-1)
         $msg .= " <span>...</span> ";

      for ($i=($strona+$zakres) ; $i>=($strona-$zakres); $i--) {
         if ($i<1 || $i>$ileStron)
            continue;
         if ($i != $strona)
            $msg .= "<a class=\"tag\" href=\"{$adres}/pageNr-{$i}\">{$i}</a>";
         else
            $msg .= "<a class=\"tag active\" href=\"{$adres}/pageNr-{$i}\">{$i}</a>";
      }


      if ($strona - $zakres > 2)
         $msg .=" <span>...</span> ";

      if ($strona - $zakres > 1)
         $msg .= "<a class=\"tag\" href=\"{$adres}/pageNr-1\">1</a> ";

      $msg.="
         <p class=\"napis\">
            Strona:
         </p>
      </div>
      <div style=\"clear:both\"></div>
      ";

      if ($ileStron=="" || $ileStron==1)
         return "";
      return $msg;
   }

   //=================================================
   // WERSJE JEZYKOWE

   public function wersjeJezykowe($obj, $jezyki, $pokazJezyk = NULL, $classPrefix = "") {
      $naglowek = "";
      $tresc = "";
      if ($pokazJezyk=="")
         $pokazJezyk = Jezyki::domyslny();

      foreach ($jezyki as $id=>$skrot) {
         $naglowek.="<a class=\"tab" . ($pokazJezyk==$id ? " active" : "") . "\" href=\"Javascript:;\" onclick=\"_tabs(this,'{$classPrefix}jezyk','{$id}')\"> &nbsp; {$skrot} &nbsp; </a>";
         $tresc .= "
            <div class=\"tabContent {$classPrefix}jezyk {$classPrefix}jezyk{$id}\" style=\"" . ($pokazJezyk==$id ? "" : "display:none") . "\">
               " . $obj->wersjaJezykowa($id) . "
            </div>
         ";
      }

      return "<div>".$naglowek."</div>".$tresc;
   }

	public function wersjeJezykoweRazem($obj, $jezyki) {
	  $tresc = "";

	  foreach ($jezyki as $i=>$w) {
	     $tresc .= "
         <h5>Język: {$w['skrot']}</h5>
         " . $obj->wersjaJezykowa($w['id']) . "

         ";
	  }

	  return $tresc;
	}

	//===========================================================================
	// pomocnicze funkcje do zarządzania galerią zdjęć

   private static $galleryScriptIncluded = false;

   public function galeriaZdjec($imgContainer, $fieldPrefix, $addImageScript = '', $hideDescription = false) {

      $msg.="
      <div id=\"zdjecia{$imgContainer->var}\" class=\"galeriaContainer\">
         <input type=hidden name=\"{$fieldPrefix}[{$imgContainer->dataName}]['dummy']}\" value=\"dummy\">
      ";
      if (count($imgContainer->data)==0)
         $msg.="<p class=\"zdjecia{$imgContainer->var}BrakZdjec\">Brak zdjęć</p>";
      else {
         $msg.="
         <p class=\"info\" style=\"margin-bottom:10px\">
            Aby zmienić kolejność zdjęć wykonaj operację 'drag and drop'.
            <br />
            Pamiętaj, aby kliknąć przycisk zapisz, aby wykonane operacje zostały zapisane.
         </p>
         ";
      }
      $msg.="
         <div id=\"zawartoscGalerii{$imgContainer->var}\" class=\"zawartoscGalerii\" style=\"margin-bottom:10px\">
         ";
         foreach ($imgContainer->data as $i=>$img)
            $msg.= self::imageDiv($img, $imgContainer, $fieldPrefix, $hideDescription);
      $msg.="
         </div>
         <div style=\"clear:both\"></div>
         ";
      //dodawanie nowych zdjęć
      $pozostaloZdjec = 999999;
      if ($imgContainer->maxZdjec!="")
         $pozostaloZdjec = $imgContainer->maxZdjec-count($imgContainer->data);

      if ($pozostaloZdjec>0) {
         $msg.="
         <h5>&nbsp;</h5>
         <div class=\"fieldDiv\">
            " . Helper::nazwaPola("Dodaj zdjęcia:", 150, "vertical-align:top;") . "
            <div style=\"display:inline-block;width:410px\">
               <div id=\"noweZdjecia{$imgContainer->var}\">
                  <input id=\"{$imgContainer->var}\" type=file size=30 name=\"{$imgContainer->var}[]\">
                  <div style=\"display:inline-block;border:1px solid #cccccc; vertical-align:bottom;height: 90px; margin-bottom: 10px; padding:10px; overflow: auto; width: 410px;\" id=\"imageGalleryQueue{$imgContainer->var}\"></div>
                  <div style=\"clear:both\"></div>
               </div>
            </div>
         </div>
         ";
      }

      $msg.="
         <div class=\"imagesToDelete\"></div>
      </div>
      <script>

         function deleteImage{$imgContainer->var}(divId, imageId) {
            var x=window.confirm('Jesteś pewien, że chcesz usunąć to zdjęcie?\\n\\nNależy zapisać zmiany, aby zdjęcie zostało faktycznie usunięte.');
            if (x) {
               $('#zdjecia{$imgContainer->var} .imagesToDelete').append('<input type=hidden name=\"{$fieldPrefix}[$imgContainer->dataName][delete][]\" value=\"'+imageId+'\">');
               $('#'+divId).remove();
            }
         }

         function addImage{$imgContainer->var}(obj) {

            $('#zawartoscGalerii{$imgContainer->var}').append(obj);
            $('.zdjecia{$imgContainer->var}BrakZdjec').hide();
            bindGalleryDescription();
         }

         $(document).ready(function() {
            $('#{$imgContainer->var}').uploadify({
               'uploader'  : '" . Helper::link("/web/admin/uploadify/uploadify.swf") . "',
               'script'    : '{$addImageScript}',
               'cancelImg' : '" . Helper::link("/web/admin/uploadify/cancel.png") . "',
               'expressInstall' : '" . Helper::link("/web/admin/uploadify/expressInstall.swf") . "',
               'auto'      : true,
               'fileDataName' : '{$imgContainer->var}',
               'fileExt'     : '*.jpg;*.gif;*.png;*.jpeg',
               'fileDesc'    : '(jpeg, png, gif)',
               'buttonText' : 'Wybierz...',
               'multi' : true,
               'queueID' : 'imageGalleryQueue{$imgContainer->var}',
               'onUploadError' : function(file, errorCode, errorMsg, errorString) {alert('The file ' + file.name + ' could not be uploaded: ' + errorString);},
               'onComplete'  : function(event, ID, fileObj, response, data) {
                  dane = jQuery.parseJSON(response);
                  if(typeof(dane.error) != 'undefined') {
   						if(dane.error != '') {
   							alert(dane.error);
   						}
                     else {
   							addImage{$imgContainer->var}(dane.msg);
   						}
   					}
               }
            });

         });

      " . (!self::$galleryScriptIncluded ? "
         function repairOrder() {
            var nowaKolejnosc = 1;
            $(galeria).find('.galleryImage').each(function(index) {
               $(this).find('.poleKolejnosc').val(nowaKolejnosc);
               nowaKolejnosc++;
            });
         }

         var repairGalleryOrder = function(e, ui) {
            galeria = $(this).parents('.galeriaContainer');
            repairOrder(galeria);
         };

         function ustawSortowanie() {
            //sortowanie
            $('.zawartoscGalerii').sortable({
               cursor: 'move', handle: 'img', stop: repairGalleryOrder});
         }

         $(document).ready(function() {
            ustawSortowanie();
         });

      " : ""
      ) . "
      </script>
      ";
      self::$galleryScriptIncluded = true;

      return $msg;
   }

   public function imageDiv($dane, $imgContainer, $fieldPrefix, $hideDescription = false) {

      $msg.="
      <div class=\"galleryImage\" id=\"image{$dane['id']}\">
         <input class=\"poleKolejnosc\" type=hidden name=\"{$fieldPrefix}[$imgContainer->dataName][{$dane['id']}][kolejnosc]\" value=\"{$dane['kolejnosc']}\">
         <img src=\"" . (self::isExternal($dane['preview']) ? "{$dane['preview']}" : "/{$dane['preview']}") . "\" " . (self::isExternal($dane['plik']) ? "width=".ADMIN_PREVIEW_WIDTH." style=\"margin-top:15px;margin-bottom:15px\"" : "") . ">
         <p style=\"font-size:12px\">
            [<a href=\"" . (self::isExternal($dane['plik']) ? "{$dane['plik']}" : "/{$dane['plik1']}") . "\" target=\"_blank\">podgląd</a>]
            &nbsp; &nbsp;
            <a style=\"float:right\" href=\"Javascript:deleteImage{$imgContainer->var}('image{$dane['id']}','{$dane['id']}')\" title=\"usuń zdjęcie\"><img src=\"/web/admin/images/trash.gif\"></a>
         </p>
         <div style=\"clear:both\"></div>
         " . (!$hideDescription ? "
         <div class=\"galleryImageDesc\">
            <span class=\"noDesc\" style=\"\">" . ($dane['opis']=="" ? "Kliknij, aby dodać opis" : $dane['opis']) . "</span>
            <textarea name=\"{$fieldPrefix}[$imgContainer->dataName][{$dane['id']}][opis]\" style=\"display:none\" class=\"galleryImageDescTextarea\">{$dane['opis']}</textarea>
         </div>
         " : ""
         ) . "
      </div>
      ";
      return $msg;
   }

   public function quickLog($to_be_logged, $logName = 'quick') {
      if (!is_string($to_be_logged))
      {
         ob_start();
         var_dump($to_be_logged);
         $loggable = ob_get_clean();
      }
      else
         $loggable = $to_be_logged;

      file_put_contents($logName . '.log', date('======== r ========') . "\n"
          . $loggable . "\n", FILE_APPEND);
   }
}
