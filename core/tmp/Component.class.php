<?php

abstract class Component {
   public $id = NULL;
   protected $baza = NULL;
   protected $tabela = NULL;
   
   protected $data = NULL;
   
   protected $possibleFields = NULL; //przycina dane przed zapisaniem do bazy do danych tu określonych; jeśli NULL to akceptuje wszystko, jeśli pusta tablica to nie przepuści nic
   public $fields = NULL;
   
   public $dirty = true;
   
   public $error = false;
   public $errors = array();
   
   public $doLogErrorByStandardCheck = TRUE;
   public $userInputErrors = array();
   
   protected $new = false;
   
   protected $readScheme; //do użycia później
   protected $operationScheme;
   
   public function exists() {
      $row = fetch_assoc($this->baza->sql("select id from {$this->tabela} where id='{$this->id}'"));
      if ($row['id']!="")
         return TRUE;
      else
         return FALSE;
   }
   
   //sql do wyszukania wiekszej liczby obiektow
   public function listFunction() {
      
   }
   
   public function setId($id) {
      $this->id = $id;
   }
   
   public function setPossibleFields($p) {
      $this->possibleFields = $p;
   }
   
   //overload
   public function __get($name) {
      if ($name=="data")
         return $this->data;
      else if ($name == "tabela")
         return $this->tabela;
      
      $trace = debug_backtrace();
      trigger_error(
         'Proba dostepu do niewidocznej zmiennej via __get(): ' . $name .
         ' in ' . $trace[0]['file'] .
         ' on line ' . $trace[0]['line'],
         E_USER_ERROR);
      return NULL;
   }
   
   public function setData($dane) {
      $this->data = $dane;
   }
   
   public function setOperationScheme($scheme) {
      $this->operationScheme = $scheme;
      $scheme->setModel($this);
   }

   //===========================================================================
   //READ
   
   //ta funkcja wczytuje dane również z 'kompontentów'
   public function getData($noReturn = false) {
      if ($this->data == NULL || $this->dirty) {
         $this->data = array();
         $this->read();
      }
      
      if (!$noReturn) {
         return $this->data;
      }
   }
   
   protected function beforeRead() {
   
   }
   
   protected function afterRead() {
   
   }
   
   protected function beforeHeaderRead() {
   
   }
   
   protected function afterHeaderRead() {
   
   }
   
   protected function doRead($data = NULL) {
   
   }
   
   protected function read($data = NULL) {
      $this->beforeRead();
      $this->doRead($data);
      $this->afterRead();
      $this->dirty = false;
   }
   
   public function init() {
   
   }
   
   public function initWithData($dane) {
      
      //muszę rozpropagować swoje id
      $this->setId($dane['id']);
      $this->init();
      
      $this->read($dane);
   }
   
   //===========================================================================
   //SPRAWDZANIE DANYCH
   
   public function standardCheck(&$dane) {
      if ($this->fields==NULL) {
         return true;
      }
      trimData($dane);
      $error = false;
      
      foreach ($dane as $id=>$wartosc) {
         if (isset($this->fields[$id]) && is_array($this->fields[$id]['check'])) {
            if (is_array($wartosc)) {
               $trace = debug_backtrace();
               trigger_error(
                  'Proba sprawdzenia poprawności danych via standardCheck(): ' . $id . ' jest tablicą! ' .
                  ' in ' . $trace[0]['file'] .
                  ' on line ' . $trace[0]['line'],
                  E_USER_ERROR);
               return;
            }
            foreach ($this->fields[$id]['check'] as $rule) {
               $name = $this->fields[$id]['name']!="" ? $this->fields[$id]['name'] : $id;
               switch ($rule['type']) {
                  case 'notnull':
                     if ($wartosc==="") {
                        $error = true;
                        
                        $msg = isset($rule['msg']) ? $rule['msg'] : "Pole '{$name}' nie może być puste";
                        if ($this->doLogErrorByStandardCheck) {
                           Log::error($msg);
                        }
                        else $this->userInputErrors[$name] = $rule['msg'];
                     }
                     break;
                  case 'maxlength':
                     if (mb_strlen($wartosc,'utf-8')>$rule['value']) {
                        $error = true;
                        $msg = isset($rule['msg']) ? $rule['msg'] : "Pole '{$name}' nie może mieć więcej niż {$rule['value']} znaków";
                        if ($this->doLogErrorByStandardCheck) {
                           Log::error($msg);
                        }
                        else $this->userInputErrors[$name] = $rule['msg'];
                     }
                     break;
                  case 'minlength':
                     if (mb_strlen($wartosc,'utf-8')<$rule['value']) {
                        $error = true;
                        $msg = isset($rule['msg']) ? $rule['msg'] : "Pole '{$name}' nie może mieć mniej niż {$rule['value']} znaków";
                        if ($this->doLogErrorByStandardCheck) {
                           Log::error($msg);
                        }
                        else $this->userInputErrors[$name] = $rule['msg'];
                     }
                     break;
                  case 'pregmatch':
                     if (preg_match("/{$rule['value']}/",$wartosc)!==1) {
                        $error = true;
                        $msg = isset($rule['msg']) ? $rule['msg'] : "Pole '{$name}' musi spełniać regułę '{$rule['value']}'";
                        if ($this->doLogErrorByStandardCheck) {
                           Log::error($msg);
                        }
                        else $this->userInputErrors[$name] = $rule['msg'];
                     }
                     break;
                  case 'email':
                     if (!isEmail($wartosc)) {
                        $error = true;
                        $msg = isset($rule['msg']) ? $rule['msg'] : "Pole '{$name}' powinno być poprawnym adresem email";
                        if ($this->doLogErrorByStandardCheck) {
                           Log::error($msg);
                        }
                        else $this->userInputErrors[$name] = $rule['msg'];
                     }
                     break;
                  case 'decimal':
                     if (!isset($rule['value'])) {
                        $rule['value'] = 2;
                     }
                     makeDecimal($dane[$id], $rule['value']);
                     $wartosc = $dane[$id];
                     break;
                  case 'strictfloat':
                     if (!Helper::isNumber($dane[$id])) {
                        $error = true;
                        $msg = isset($rule['msg']) ? $rule['msg'] : "Pole '{$name}' powinno być liczbą";
                        if ($this->doLogErrorByStandardCheck) {
                           Log::error($msg);
                        }
                        else $this->userInputErrors[$name] = $rule['msg'];
                     }
                     else {
                        makeFloat($dane[$id]);
                        $wartosc = $dane[$id];
                     }
                     break;
                  case 'shouldbeint':
                     if (!Helper::isInt($dane[$id])) {
                        $error = true;
                        $msg = isset($rule['msg']) ? $rule['msg'] : "Pole '{$name}' powinno być liczbą całkowitą - ustawiono wartość 0";
                        $dane[$id] = 0;
                        if ($this->doLogErrorByStandardCheck) {
                           Log::warning($msg);
                        }
                        //else $this->userInputErrors[$name] = $rule['msg'];
                     }
                     else {
                        makeFloat($dane[$id]);
                        $wartosc = $dane[$id];
                     }
                     break;
                  case 'float':
                     makeFloat($dane[$id]);
                     $wartosc = $dane[$id];
                     break;
                  case 'integer':
                     makeDecimal($dane[$id]);
                     $wartosc = $dane[$id];
                     break;
                  case 'lowercase':
                     $dane[$id] = mb_strtolower($dane[$id]);
                     $wartosc = $dane[$id];
                     break;
                  case 'uppercase':
                     $dane[$id] = mb_strtoupper($dane[$id]);
                     $wartosc = $dane[$id];
                     break;
                  case 'striptags':
                     $dane[$id] = strip_tags($dane[$id], $rule['value']);
                     $wartosc = $dane[$id];
                     break;
                  case 'normalize':
                     $dane[$id] = textId($dane[$id]);
                     $wartosc = $dane[$id];
                     break;
                  case 'maxvalue':
                     if ($wartosc>$rule['value']) {
                        $error = true;
                        $msg = isset($rule['msg']) ? $rule['msg'] : "Wartość '{$name}' nie może być większa niż {$rule['value']}";
                        if ($this->doLogErrorByStandardCheck) {
                           Log::error($msg);
                        }
                        else $this->userInputErrors[$name] = $rule['msg'];
                     }
                     break;
                  case 'minvalue':
                     if ($wartosc<$rule['value']) {
                        
                        $error = true;
                        $msg = isset($rule['msg']) ? $rule['msg'] : "Wartość '{$name}' nie może być mniejsza niż {$rule['value']}";
                        if ($this->doLogErrorByStandardCheck) {
                           Log::error($msg);
                        }
                        else $this->userInputErrors[$name] = $rule['msg'];
                     }
                     break;  
                   default:
                     $trace = debug_backtrace();
                     trigger_error(
                        'Proba sprawdzenia poprawności danych via standardCheck(): Nieokreślona reguła ' . $rule['type'] . '! ' .
                        ' in ' . $trace[0]['file'] .
                        ' on line ' . $trace[0]['line'],
                        E_USER_ERROR);
                     break; 
               }
            }
         }
      }
      
      return !$error;
   }
   
   public function getUserInputErrorsForJson($urlEncode = TRUE) {
      $errors = array();
      if ($urlEncode) {
         foreach ($this->userInputErrors as $name => $msg) {
            //$errors[$name] = urlencode($msg);
            $errors[$name] = rawurlencode($msg);
         }
      }
      else $errors = $this->userInputErrors;
      //$returned = array('success' => ($this->error ? '0' : '1'), 'fields' => $errors);
      $returned = array('success' => (empty($this->userInputErrors) ? '1' : '0'), 'fields' => $errors);
      return $returned;
   }
   
   public function parseUserInputErrorsToArrayForJson($userInputErrorsArray, $arrName = '') {
      if (empty($arrName)) return $userInputErrorsArray;
      
      $copy = $userInputErrorsArray;
      $errors = $copy['fields'];
      unset($copy['fields']);
      $returned = $copy;
      $fieldsInArr = array();
      foreach ($errors as $name => $msg) {
         $key = "{$arrName}[{$name}]";
         $fieldsInArr[$key] = $msg;
      }
      $returned['fields'] = $fieldsInArr;
      return $returned;
   }
   
   
   public function doCheck(&$dane, $update = false) {
      if (!$this->standardCheck($dane)) {
         $this->error = true;
      }
   
      if (!$this->checkData($dane, $update)) {
         $this->error = true;
      }
      
      $this->trimToPossibleFields($dane);
   }
   
   //musi zwracać true lub false
   protected function checkData(&$dane, $update = false) {
      //to powinno być nadpisane, aby wykonać weryfikację danych
      
      return true;
   }
   
   protected function trimToPossibleFields(&$dane) {
      trimToPossibleFields($dane, $this->possibleFields);
   }
   
   // MK - dodałem - nie można data ustawiać ani pobierać, bo jest PROTECTED
   public function setDataVar($varName, $value) {
      $this->data[$varName] = $value;
   }

   public function getDataVar($varName) {
      return $this->data[$varName];
   }
   
   //===========================================================================
   //UPDATE
   
   protected function beforeUpdate(&$dane) {
   
   }
   
   protected function afterUpdate(&$dane) {
   
   }
      
   public function doUpdate($data) {
   
   }
   
   public function update(&$data) {
      if ($this->operationScheme!=NULL) {
         return $this->operationScheme->update($data);
      }
   
      //dane muszą być tablicą!
      if (!is_array($data))
         return false;
         
      $this->doCheck($data, true);
      
      //jeśli check zwrócił błąd, to kończymy z błędem
      if ($this->error)
         return false;
         
      $this->updateAfterCheck($data);
      
      return true;
   }
   
   //wywołanie tej funkcji zakłada, że sprawdzenie danych zostało wcześniej wykonane
   public function updateAfterCheck(&$data) {
      $this->beforeUpdate($data);
      
      $this->doUpdate($data);
      
      $this->afterUpdate($data);
      $this->dirty = true;
   }
   
   //===========================================================================
   //ADD
   
   protected function beforeAdd(&$dane) {
   
   }
   
   protected function afterAdd(&$dane) {
   
   }
   
   public function doAdd($data) {
   
   }
   
   public function add(&$data) {
      if ($this->operationScheme!=NULL) {
         return $this->operationScheme->add($data);
      }
      
      //dane muszą być tablicą!
      if (!is_array($data))
         return false;
         
      $this->doCheck($data, false);
      
      //jeśli check zwrócił błąd, to kończymy z błędem
      if ($this->error) {
         $this->data = $data;
         $this->dirty = false;
         return false;
      }
         
      $this->addAfterCheck($data);
      
      return true;
   }
   
   //wywołanie tej funkcji zakłada, że sprawdzenie danych zostało wcześniej wykonane
   public function addAfterCheck($data) {
      $this->beforeAdd($data);
               
      $this->doAdd($data);
      
      //zmieniam swój id
      if ($data['id']!="")
         $this->setId($data['id']);
      else
         $this->setId(BazaDanych::$insertId);
      
      $this->afterAdd($data);
      $this->dirty = true;
   }
   
   //===========================================================================
   //DELETE
   
   protected function beforeDelete() {
   
   }
   
   protected function afterDelete() {
   
   }
   
   public function doDelete() {
   
   }
   
   public function delete() {
      if ($this->operationScheme!=NULL) {
         return $this->operationScheme->delete();
      }
      
      $this->getData(true);
      $this->beforeDelete();
      
      //jeśli w beforeDelete wyskoczył błąd
      if ($this->error)
         return false;
      
      $this->doDelete();
      
      $this->dirty = true;
      
      $this->afterDelete();
      
      return true;
   }

   public function patch_update(&$dane) {
      $this->updateAfterCheck($dane);
   }
}

