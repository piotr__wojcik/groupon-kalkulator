<?php

class BazaDanych extends MYSQLi {

   public static $insertId = NULL;
   private static $instance = NULL;
   private static $isConnected = false;
   
   public static function dajBaze() {
      if (self::$instance == NULL) self::$instance = new BazaDanych();
      return self::$instance;
   }
   
   public static function escapeString($text) {
      $baza = self::dajBaze();
      if (!self::$isConnected) {
         $baza->connectWithDB();
      }
      return $baza->real_escape_string($text);
   }
   
   private function __construct() {

   }
    
   public function __wakeup() {
      $this->isConnected = false;
   }
   
   private function connectWithDB() {
      //ob aby nie wywalało błędów o braku połączenia z bazą
      ob_start();
      parent::__construct(Config::get()->DB_HOST, Config::get()->DB_USER, Config::get()->DB_PASS, Config::get()->DB_NAME, Config::get()->DB_PORT);
      $warnings = ob_get_contents();
      ob_end_clean();
      
      //wykrywamy czy jest baza
      if ($warnings!="") {
         if (Config::get()->TEST_SERVER===true) {
            die('BLAD POLACZENIA Z BAZA DANYCH');
         }
         else {
            die('BLAD POLACZENIA Z BAZA DANYCH');
         }
      }
      $this->set_charset('utf8');
      if ($this->errno > 0) 
         die('BŁĄD połączenia z bazą danych: ' . $this->error);
      
      self::$isConnected = true;
      
   }
   
   
   
   /*
   MYSQLI_STORE_RESULT - do operowania BUFOROWANEGO, gdy NIE pytamy o duza ilosc danych. 
      Mozna potem wygrzebywac dane z konkretnego rzedu, szukajac ich poprzez funkcje mysqli_data_seek().
   MYSQLI_USE_RESULT - do operowania BEZ BUFOROWANIA, gdy pytamy o duza ilosc kolumn. 
      Nie umozliwa szukania danych poprzez funkcje mysqli_data_seek()
      UWAGA: blokuje innym watkom mozliwosc update'owania tabel, z ktorych pobierane sa dane
   */
   
   private function get_correct_utf8_mysql_string($s)
   {
       return preg_replace( "#\xC2\xA3#x" , "&pound;" , $s);
   } 

   public function sql($sql, $resultMode = MYSQLI_STORE_RESULT) {
      //trace();
      if (!self::$isConnected) {
         $this->connectWithDB();
      }
   
      $sql = $this->get_correct_utf8_mysql_string($sql);
   
      $wynik = $this->query($sql, $resultMode);
      
      if ($this->errno > 0) {
         if (TEST_SERVER===true) {
            die('BLAD zapytania: ' . $this->errno . '<br />'.$sql.'<br /><br />' . $this->error);
         }
         else {
            die('BLAD zapytania: ' . $this->errno . '<br />'.$sql.'<br /><br />' . $this->error);
         }
         return NULL;
      }
      
      if ($this->insert_id != 0)
         self::$insertId = $this->insert_id;

      return $wynik;
   }
   
   /*
   Funkcja wykonujaca jednoczesnie WIELE zapytan SQL, ktore powinny byc oddzielone srednikiem ;
   */
   
   public function sqlMulti($sqlQuery) {
      if (!$this->isConnected) {
         $this->connectWithDB();
      }
      
      $wynik = $this->multi_query($sqlQuery);
      
      if ($this->errno > 0) {
         die('BŁĄD zapytania: ' . $this->errno());
         return $this->error;
      }
      
      return $wynik;
   }
   
   public function sqlMultiNextRow($buforowanie = TRUE) {
      if (!$this->isConnected) {
         $this->connectWithDB();
      }
      
      $this->next_result();
      return $buforowanie ? $this->store_result() : $this->use_result();
   }
   
   public function freeResult($wynik) {
      if (!$this->isConnected) {
         $this->connectWithDB();
      }
      
      mysqli_free_result($wynik);
   }
   
   public function reset() {
      self::dajBaze();
      if (!$this->isConnected) {
         $this->connectWithDB();
      }
      while ($wynikKolejny = $this->nastepnyWynik()) {
         mysqli_free_result($wynikKolejny);
      }
   }
}

