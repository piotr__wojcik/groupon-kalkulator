<?php

class Lista {
   protected $class;
   protected $polecenie;
   protected $baza;
   protected $onPage;
   protected $page;
   public $data = NULL;
   
   public function __construct($class, $polecenie, $page, $onPage = 10, $largeData = false) {
      $this->class = $class;
      $this->polecenie = $polecenie;
      $this->page = $page;
      $this->onPage = $onPage;
      $this->baza = BazaDanych::dajBaze();
      $this->largeData = $largeData;
      $this->getData(true);
   }
   
   public function getData() {

      if ($this->data == NULL)
         $this->read();

      return $this->data;
   }
   
   protected function read() {
      if ($this->largeData) {
         $wynik = $this->page*$this->onPage+$this->onPage*10;
      }
      else {
         $wynik = num_rows($this->baza->sql($this->polecenie));
      }
      
      $this->data['pages'] = ceil($wynik/$this->onPage);
      $this->data['page'] = $this->page;
      $this->data['numElements'] = $wynik;
      
      $this->data['elements'] = array();
      
      $wynik =$this->baza->sql($this->polecenie." limit " . (($this->page-1)*$this->onPage). ", " . $this->onPage);
      
      while ($row = fetch_assoc($wynik)) {
         $pom = new $this->class($row['id']);
         $pom->getData(true);
         $this->data['elements'][] = $pom;
      }

   }
      
}
   
