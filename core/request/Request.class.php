<?php

final class Request {
   
   //static properties (this is a singleton)
   
   static private $instance = NULL;
   
   //to init singleton
   static public function init() {
      if (self::$instance == NULL) {
         self::$instance = new Request();
      }
   }
   
   //getter for config object
   static public function get() {
      return self::$instance;
   }
   
   //object properties (this is a singleton)
   
   public $raw;
   public $post;
   public $get;
   public $files;
   public $pageNr = 1;
   public $ajax = false;
   public $method = "";
   
   //private constructor
   private function __construct() {
      $this->raw = new stdClass;
      $this->raw->post = $_POST;
      $this->raw->get = $_GET;
      $this->raw->files = $_FILES;
      
      $this->post = $_POST;
      $this->get = $_GET;
      if (!isset($this->get['pageNr'])) {
        $this->get['pageNr'] = 1;
      }
      $this->files = $_FILES;
      $this->method = $_SERVER['REQUEST_METHOD'];
      $this->input = file_get_contents('php://input');
   }
   
   private function cleanRequest() {
      $requestCleanerName = Config::get()->REQUEST_CLEANER;
      if ($requestCleanerName=="" || !class_exists($requestCleanerName)) {
         die("FAILED TO INIT FRAMEWORK: {$requestCleanerName} request cleaner class not found");
      }
      $requestCleaner = new $requestCleanerName();
      $requestCleaner->init();
   }
   
}
