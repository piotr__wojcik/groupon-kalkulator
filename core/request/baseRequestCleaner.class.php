<?php
    
class baseRequestCleaner implements IRequestCleaner {

   public function init() {
      //we only clean post data
      foreach (Request::get()->raw->post as $i => $w) {
         Request::get()->post[$i] = stripslashes(strip_tags($w));
      }
   }
   
}
