<?php

abstract class baseController {

   public $defaultMethod = "show";

   public function beforeExecute() {
   
   }

   public function afterExecute() {
   
   }
}
