<?php
    
class baseInitHandler implements IInitHandler {

   protected $contextDetector = NULL;
   protected $routerHandler = NULL;

   public function init() {
      //first deal with session
      $this->startSession();
      //then parse the request (only parse, do not route yet, cause context may need some data from request)
      $this->parseRequest();
      //detect context of current request (before router is enabled)
      $this->detectContextAfterParsingRequest();
      //route
      $this->runRouter();
      //detect context after router is enabled (before router is enabled)
      $this->detectContextAfterRouting();
   }
   
   public function startSession() {
      if (Config::get()->USE_SESSION) {
         Session::init();
      }
   }
   
   public function parseRequest() {
   
   }
   
   public function detectContextAfterParsingRequest() {
      //init the context singleton
      Context::init();
      //check whether context detector exists
      $contextDetectorName = Config::get()->CONTEXT_DETECTOR;
      if ($contextDetectorName=="" || !class_exists($contextDetectorName)) {
         die("FAILED TO INIT APP: {$contextDetectorName} context handler class not found");
      }
      //create context detector (we will use it later, so we'll need it as a property
      $this->contextDetector = new $contextDetectorName();
      //run the proper method
      $this->contextDetector->detectContextAfterParsingRequest();
   }
   
   public function execute() {
      //route
      $this->runRouter();
      //detect context after router is enabled (before router is enabled)
      $this->detectContextAfterRouting();
      //execute the proper controller
      $this->beforeExecute();
      $this->doExecute();
      $this->afterExecute();
   }
   
   public function runRouter() {
      $routerHandlerName = Config::get()->ROUTER_HANDLER;
      if ($routerHandlerName=="" || !class_exists($routerHandlerName)) {
         die("FAILED TO INIT APP: {$routerHandlerName} router handler class not found");
      }
      //create router handler (we will use it later, so we'll need it as a property
      $this->routerHandler = new $routerHandlerName();
      //run the proper method
      $this->routerHandler->translateRequest();
   }
   
   public function detectContextAfterRouting() {
      //we do not need to create the context handler or check it's existence, cuase this was done in detectContextAfterParsingRequest()
      //run the proper method
      $this->contextDetector->detectContextAfterRouting();
   }
   
   public function beforeExecute() {
      //we do not need to create the router handler or check it's existence, cuase this was done in runRouter()
      //run the proper method
      $this->routerHandler->beforeExecute();
   }
   
   public function doExecute() {
      //we do not need to create the router handler or check it's existence, cuase this was done in runRouter()
      //run the proper method
      echo $this->routerHandler->doExecute();
   }
   
   public function afterExecute() {
      //we do not need to create the router handler or check it's existence, cuase this was done in runRouter()
      //run the proper method
      $this->routerHandler->afterExecute();
      //save session
      if (Config::get()->USE_SESSION) {
         Session::save();
      }
   }
}
