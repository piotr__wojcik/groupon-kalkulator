<?php

/*
this works in two modes: 
- if on production server, then we look at the LAST_MODIFIED timestamp to be sure that we have a current list
- if on dev server, then we simply load the class
*/

function __BI_autoload($classname) {
   global $__CLASS_LIST, $__CLASS_LIST_WARNINGS;
   
   $classname = strtolower($classname);
   $classListName = __BI_autoload__class_list_name();
   if (!file_exists($classListName)) {
      __BI_autoload_create_class_list(Config::get()->PROD_SERVER);
   }
   require_once($classListName);
   
   if (Config::get()->PROD_SERVER) {
      require_once($__CLASS_LIST[$classname]);
   }
   else {
      //try to reload class list if we can't find our class in the current one
      if (!isset($__CLASS_LIST[$classname])) {
         $classListName = __BI_autoload_create_class_list(Config::get()->PROD_SERVER);
      }
      //try to load class
      if (isset($__CLASS_LIST[$classname])) {
         require_once($__CLASS_LIST[$classname]);
      }
      else if (isset($__CLASS_LIST_WARNINGS[$classname])) {
         require_once($__CLASS_LIST_WARNINGS[$classname]);
         //trigger_error("Class {$classname} has no namespace defined or the defined namespace is not correct - this will not work on production server", E_USER_WARNING);
      }
   }
}

spl_autoload_register('__BI_autoload');

function __BI_autoload_create_class_list($prodList) {
   $arr = array('proper'=>array(),'notproper'=>array());
   __BI_autoload__scan_dir(BI::$appDir, "", "", $arr);
   __BI_autoload__scan_dir("lib", "", "", $arr);
   __BI_autoload__scan_dir("core", "", "", $arr);
   
   $classListName = __BI_autoload__class_list_name();
   $fp = fopen($classListName, 'w');
   fwrite($fp,"<?php\n");
   fwrite($fp,chr(36) . "__CLASS_LIST = array();\n");
   foreach ($arr['proper'] as $id=>$w) {
      fwrite($fp, chr(36) . "__CLASS_LIST['{$id}']=\"{$w}\";\n");
   }
   if (!$prodList) {
      fwrite($fp,chr(36) . "__CLASS_LIST_WARNINGS = array();\n");
      foreach ($arr['notproper'] as $id=>$w) {
         fwrite($fp, chr(36) . "__CLASS_LIST_WARNINGS['{$id}']=\"{$w}\";\n");
      }
   }
   fclose($fp);  
   return $arr['proper'];
}

function __BI_autoload__class_list_name() {
   return Config::get()->APP_NAME.'-class-list.'.LAST_MODIFIED.'.php';
}

function __BI_autoload__scan_dir($dirname, $path = "", $namespace = "", &$arr) {
   if (substr($dirname,-1,1)=="/") {
      $dirname = substr($dirname,0,-1);
   }
   $path .= $dirname.'/';
   
   $namespace .= ($namespace!='' ? '\\' : '').$dirname;
   
   if ($handle = opendir($path)) {
      $dirsToVisit = array();
      while (false !== ($file = readdir($handle))) { 
         if ($file != "." && $file != "..") {
            if (is_dir($path.$file)) {
               $dirsToVisit[] = array($file, $path);
            }
				else if (is_file($path.$file)) {
               if (substr($file,-4)==".php") {
                  if (substr($file,-10)==".class.php") {
                     $foundClass = strtolower(substr($file,0,-10));
                  } else {
                     $foundClass = strtolower(substr($file,0,-4));
                  }
                  $fullClassName = strtolower($namespace.'\\'.$foundClass);
                  if (substr($namespace,0,4)=="core") {
                     $arr['proper'][$foundClass] = $path.$file;
                  }
                  else {
                     $arr['proper'][$fullClassName] = $path.$file;
                     if ( ($pos = strpos($foundClass,'controller'))!==FALSE) {
                        $arr['proper'][$foundClass] = $path.$file;
                     }
                     $arr['notproper'][$foundClass] = $path.$file;
						}
               }
            }
         }
      }
      closedir($handle); 
      foreach ($dirsToVisit as $i=>$w) {
         __BI_autoload__scan_dir($w[0], $w[1], $namespace, $arr);
      }
   }
}
