<?php
// PHPArrayConfigLoader - all the config data has to be in a php array named $__CONFIG

namespace lib\core\configLoaders;
use Config, 
    IConfigLoader;

class PHPArrayConfigLoader implements IConfigLoader {
   
   private $configObject;
   
   public function setWrapper(Config $configObject) {
      $this->configObject = $configObject;
   }
   
   public function readConfig($fileName) {
      //add the fiel extension
      $fileName = $fileName.".php";
      
      //ensure that the file exists
      if (!file_exists($fileName)) {
         die("FAILED TO LOAD CONFIGURATION FILE: PHPArrayConfigLoader->readConfig() - {$fileName} file missing");
      }
      
      require_once($fileName);
      
      if (isset($__CONFIG) && is_array($__CONFIG)) {
         $this->configObject->appendConfiguration($__CONFIG);
      }
   }
   
}
