<?php

namespace lib\core\sessionHandlers;

use ISessionHandler;

class PHPSessionHandler implements ISessionHandler {
   
   public function init() {
      session_start();
   }
   
   public function __set($name, $value) {
      $_SESSION[$name] = $value;
   }
   
   public function &__get($name) {
      return $_SESSION[$name];
   }
   
   public function __isset($name) {
      isset($_SESSION['name']);
   }
   
   public function __unset($name) {
      unset($_SESSION['name']);
   }
   
   public function save() {
      //$_SESSION saves everything instantly
   }
   
}
