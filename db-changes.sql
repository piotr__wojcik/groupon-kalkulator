-- 06.01.2017

CREATE TABLE `editable_pages` (
  `id` varchar(20) NOT NULL COMMENT 'should be the same as the name of the controller',
  `last_modified` datetime NOT NULL,
  `link_name` varchar(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `editable_pages`
  ADD PRIMARY KEY (`id`);

INSERT INTO `editable_pages` (`id`, `last_modified`, `link_name`, `title`, `content`) VALUES
('subscriber', '2017-01-07 14:51:25', 'link', 'Subskrybent', '<p><img alt="" src="/web/public/kcfinder/images/subskrybent/subscriber-1.png" /></p>\r\n<p><img alt="" src="/web/public/kcfinder/images/subskrybent/subscriber-2.png" /></p>\r\n\r\n<p><img alt="" src="/web/public/kcfinder/images/subskrybent/subscriber-3.png" /></p>');

ALTER TABLE `instruments` ADD `faq` TEXT NOT NULL AFTER `data`;

ALTER TABLE `offers` ADD `user_comment` TEXT NOT NULL AFTER `summary_settings`, ADD `comment` TEXT NOT NULL AFTER `user_comment`;

-- 21.03.2017
