function confirmDelete(goToLocation) {
    confirmAction('Czy jesteś pewien, że chcesz usunąć tą pozycję?\n\nTa akcja jest nieodwracalna!', goToLocation);
}

function confirmForm(form) {
    var x = window.confirm('Czy jesteś pewien?\n\nTa akcja jest nieodwracalna!');
    if (x) {
        $(form).submit();
    }
}

function confirmOrderCancel(goToLocation) {
    var x = window.confirm('Czy jesteś pewien?\n\nTa akcja jest nieodwracalna i nie spowoduje anulowania przypisanej do zamówienia subskrypcji - tę operację musisz wykonać osobno');
    if (x) {
        location = goToLocation;
    }
}

function confirmPlanCancel(goToLocation) {
    var x = window.confirm('Czy jesteś pewien?\n\nTa akcja jest nieodwracalna i nie spowoduje anulowania ew. przypisanego zamówienia - jeśli trzeba zwrócić środki, operację tę musisz wykonać osobno');
    if (x) {
        location = goToLocation;
    }
}

function confirmAction(msg, goToLocation) {
    var x = window.confirm(msg);
    if (x) {
        location = goToLocation;
    }
}

function confirmAction(msg, goToLocation) {
    var x = window.confirm(msg);
    if (x) {
        location = goToLocation;
    }
}

$(document).ready(function () {

    //ckeditor
    CKEDITOR.replaceAll( 'standardCkeeditor', {
        height: 600,
        width: 1019,
        customConfig : '/web/admin/js/ckeConfig.js'
        //mozemy jeszcze stad zmieniac config
        //contentsCss : '" . staticLink("/web/admin/ckeStyle2.css") . "'
    });
    CKEDITOR.replaceAll( 'smallCkeeditor', {
        height: 400,
        width: 620,
        customConfig : '/web/admin/js/ckeConfig.js'
        //mozemy jeszcze stad zmieniac config
        //contentsCss : '" . staticLink("/web/admin/ckeStyle2.css") . "'
    });
   
    //datepicker
    if ($(".datepicker").length)
    {
        $(".datepicker").datepicker({
            dateFormat: "yy-mm-dd"
        });

        var da = '';
        $(".datepicker").each(function () {
            if (typeof ($(this).data('min-date')) !== "undefined" && $(this).data('min-date') != "")
            {
                da = $(this).data('min-date').split('-');
                $(this).datepicker("option", "minDate", new Date(parseInt(da[0]), parseInt(da[1]) - 1, parseInt(da[2])));
            }
            if (typeof ($(this).data('max-date')) !== "undefined" && $(this).data('max-date') != "")
            {
                da = $(this).data('max-date').split('-');
                $(this).datepicker("option", "maxDate", new Date(parseInt(da[0]), parseInt(da[1]) - 1, parseInt(da[2])));
            }
        });
    }
    
    //no enter allowed
    $('.oneline').keypress(function (event) {
        if (event.which == '13') {
            $(this).blur();
            return false;
        }
    });

    //pokazywanie panelu hasla w edycji admina
    $('.AdminAccount-googleOnlyLoginRadio').change(function () {
        if ($(this).val() == '1') {
            $('#AdminAccount-PasswordDiv').slideUp();
        } else {
            $('#AdminAccount-PasswordDiv').slideDown();
        }
    });

    //klonowanie
    $(".cloneElement").on("click", cloneElement);
    $(".removeClonedElement").on("click", removeClonedElement);

    //wrappery wariantu instrumentu
    if ($('.instrumentVariantTypeRadio').length) {
        $('.instrumentVariantTypeRadio').change(instrumentVariantTypeChange);
        instrumentVariantTypeChange();
    }

    //dodawanie opcji
    $('.addOption').click(function () {
        var howMany = $('.optionWrapper:visible').length;
        if (howMany >= 20) {
            return;
        }
        howMany++;
        if (howMany > 5) {
            $('.optionRow2').show();
        }
        if (howMany > 10) {
            $('.optionRow3').show();
        }
        if (howMany > 15) {
            $('.optionRow4').show();
        }
        $('.optionWrapper' + howMany).show().find('.usageField').val(1);
        //kopiujemy prowizje
        if (howMany > 1) {
            var previousCommission = $('.optionWrapper' + (howMany - 1)).find('input[data-class="row6"]').val() * 1;
            if (isFinite(previousCommission)) {
                $('.optionWrapper' + howMany).find('input[data-class="row6"]').val(previousCommission);
            }

        }
    });

    $('.removeOption').click(function () {
        var x = window.confirm('Czy jesteś pewien?');
        if (x) {
            //szukamy czy są opcje widoczne na prawo od naszej
            var optionId = $(this).parents('.optionWrapper').data('option-id') * 1;
            $(this).parents('.optionWrapper').hide().find('.usageField').val(0);
            for (var i = optionId; i < $('.optionWrapper').length; i++) {
                var nextOptionWrapper = $('.optionWrapper' + (i + 1));
                if ($(nextOptionWrapper).find('.usageField').val() == '1') {
                    deleteCurrentOption = false;
                    copyOption(nextOptionWrapper, $('.optionWrapper' + i));
                    $(nextOptionWrapper).hide().find('.usageField').val(0);
                }
            }

            //hide second row
            var howMany = $('.optionWrapper:visible').length;
            if (howMany < 6) {
                $('.optionRow2').hide();
            }
            if (howMany < 11) {
                $('.optionRow3').hide();
            }
            if (howMany < 16) {
                $('.optionRow4').hide();
            }

            ofertaPrzeliczOpcje3($(this).parents('.optionWrapper').find('.usageField'));
        }
    });

    //edycja nazwy opcji
    $('.optionNameSpan').click(function () {
        $(this).hide();
        $(this).parents('label').find('.optionNameField').show();
    });
    $('.optionNameField').blur(function () {
        $(this).hide();
        $(this).parents('label').find('.optionNameSpan').html($(this).val()).show();
    });

    $('.optionWrapper .message').click(function () {
        $(this).hide();
    });

    if ($('.optionsRow').length) {
        $('.optionsRow input').focus(function () {
            $(this).parents('.optionsRow').find('.' + $(this).data('class')).addClass('highlight');
        }).blur(function () {
            $(this).parents('.optionsRow').find('.' + $(this).data('class')).removeClass('highlight');
        });

        //edycja ceny normalnej i grouponu
        $('.optionsRow input').keypress(function (event) {
            //8 ok, 0 ok
            if (event.which == 44 || event.which == 188 || event.which == 46) {
                return true;
            }
            if (
                    (event.which < 48 || event.which > 57)
                    && event.which != 8
                    && event.which != 0
                    ) {
                return false;
            }
        });
        $('.optionsRow input').keyup(function (event) {
            //8 ok, 0 ok
            if (event.which == 44 || event.which == 188) {
                $(this).val($(this).val().replace(',', '.'));
            }

        });

        //rabat
        $('input[data-class="row1"],input[data-class="row2"]').keyup(function () {
            ofertaPrzeliczOpcje1($(this));
        });

        //koszt produktu
        $('input[data-class="row1"],input[data-class="row5"]').keyup(function () {
            ofertaPrzeliczOpcje2($(this));
        });

        //przelew dla partnera produktu
        $('input[data-class="row1"],input[data-class="row2"],input[data-class="row5"],input[data-class="row6"],input[data-class="row7"],input[data-class="row11"]').keyup(function () {
            ofertaPrzeliczOpcje3($(this));
        });

        //przelew dla partnera produktu
        $('input[data-class="summary3"],input[data-class="summary4"]').keyup(function () {
            ofertaPrzeliczSumy();
        });

        //na start
        $('.optionWrapper:visible input[data-class="row1"]').each(function () {
            ofertaPrzeliczOpcje1($(this));
            ofertaPrzeliczOpcje2($(this));
            ofertaPrzeliczOpcje3($(this));
        });
    }

    $('.selectOnFocus').focus(function () {
        $(this).select();
    });

    $('.clipboardCopy').click(function () {
        copyTextToClipboard($(this).data('url'));
    });

    //autozapis formularzy oferty
    $('.offerNaviLink').click(function (e) {
        if ($('form').length) {
            e.preventDefault();
            $('form').attr('action', '?redirect=' + $(this).attr('href'));
            $('form').submit();
        }
    });

    //rozwijanie wierszy oferty
    $('.hiddenSectionToggle').click(function () {
        $('.hidden-section-' + $(this).data('section-id')).slideToggle();
    });

    $('.summaryTrigger').change(function () {
        var showClass = $(this).data('show');
        var hideClass = showClass + 'Hide';
        if ($(this).is(':checked')) {
            $('.' + showClass).show();
            $('.' + hideClass).hide();
            if (showClass == 'showOptions') {
                $('.summaryTrigger').prop('checked', true);
            }
        } else {
            $('.' + showClass).hide();
            $('.' + hideClass).show();
        }
    });
});

var copyOption = function (from, to) {
    $(from).find('input').each(function () {
        var myClass = $(this).data('class');
        $(to).find('input[data-class="' + myClass + '"]').val($(this).val());
    });
    //nazwa opcji
    $(to).find('.optionNameField').val($(from).find('.optionNameField').val());
    $(to).find('.optionNameSpan').html($(from).find('.optionNameSpan').html());
    $(to).show().find('.usageField').val(1);
}

var ofertaPrzeliczOpcje1 = function (obj) {
    var row1 = $(obj).parents('.optionWrapper').find('input[data-class="row1"]').val();//cena normalna
    var row2 = $(obj).parents('.optionWrapper').find('input[data-class="row2"]').val();//cena grp
    var row3 = $(obj).parents('.optionWrapper').find('input[data-class="row3"]');
    var val = row2 / row1 - 1;
    if (isFinite(val)) {
        val = Math.round(val * 100 * 100) / 100;
        $(row3).val(val + '%');
        if (val > 0) {
            $(row3).addClass('error');
        } else {
            $(row3).removeClass('error');
        }
    } else {
        $(row3).val('');
    }
}

var ofertaPrzeliczOpcje2 = function (obj) {
    var row1 = $(obj).parents('.optionWrapper').find('input[data-class="row1"]').val();//cena normlna
    var row2 = $(obj).parents('.optionWrapper').find('input[data-class="row5"]').val();//koszt
    var row3 = $(obj).parents('.optionWrapper').find('input[data-class="row4"]');//koszt %
    var val = (row2 / row1) * 100;

    if (isFinite(val)) {
        val = Math.round(val * 100) / 100;
        $(row3).val(val + '%');

        if (row2 * 1 > row1 * 1) {
            $(row3).addClass('error');
        } else {
            $(row3).removeClass('error');
        }
    } else {
        $(row3).val('');
    }
}


var ofertaPrzeliczOpcje3 = function (obj) {
    var row1 = $(obj).parents('.optionWrapper').find('input[data-class="row1"]').val();//cena norm
    var row1a = $(obj).parents('.optionWrapper').find('input[data-class="row2"]').val();//cena grp
    var row1b = $(obj).parents('.optionWrapper').find('input[data-class="row5"]').val();//koszt
    var row2 = $(obj).parents('.optionWrapper').find('input[data-class="row6"]').val();//prowizja grp
    var row2a = $(obj).parents('.optionWrapper').find('input[data-class="row7"]').val();//vat grp
    var row2b = $(obj).parents('.optionWrapper').find('input[data-class="row11"]').val();//l. grp

    var row3 = $(obj).parents('.optionWrapper').find('input[data-class="row8"]');
    var row3a = $(obj).parents('.optionWrapper').find('input[data-class="row9"]');
    var row3b = $(obj).parents('.optionWrapper').find('input[data-class="row10"]');
    var row3c = $(obj).parents('.optionWrapper').find('input[data-class="row12"]');
    var row3d = $(obj).parents('.optionWrapper').find('input[data-class="row13"]');
    var row3e = $(obj).parents('.optionWrapper').find('input[data-class="row14"]');

    var val =
            (
                    row1a -
                    (
                            row1a
                            * (row2 / 100)
                            * ((100 + parseInt(row2a)) / 100)
                            )
                    );

    if (isFinite(val)) {
        //dla partnera
        val = Math.round(val * 100) / 100;
        $(row3).val(val);
        if (val < 0) {
            $(row3).addClass('error');
        } else {
            $(row3).removeClass('error');
        }
        //roznica od 100%
        val2 = row1 - val;
        if (isFinite(val2)) {
            $(row3a).val(Math.round(val2 * 100) / 100);
        } else {
            $(row3a).val('');
        }
        //zysk
        val3 = val - row1b;
        if (isFinite(val3)) {
            $(row3b).val(Math.round(val3 * 100) / 100);
        } else {
            $(row3b).val('');
        }
        //suma dla partnera
        val4 = val * row2b;
        if (isFinite(val4)) {
            $(row3c).val(Math.round(val4 * 100) / 100);
        } else {
            $(row3c).val('');
        }
        //zysk dla partnera
        val5 = val2 * row2b;
        if (isFinite(val5)) {
            $(row3d).val(Math.round(val5 * 100) / 100);
        } else {
            $(row3d).val('');
        }

        //zysk dla grp
        val6 = row1a * (row2 / 100) * row2b;
        if (isFinite(val6)) {
            $(row3e).val(Math.round(val6 * 100) / 100);
        } else {
            $(row3e).val('');
        }

        //sumy
        ofertaPrzeliczSumy();

    } else {
        $(row3).val('');
    }

    //sprawdzamy jeszcze prowizje GRP
    var prowizja = row2 * 1;
    var notice = '';
    var error = '';
    var from, to = 0;
    for (var i in CATEGORY_RULES) {
        from = CATEGORY_RULES[i].from * 1;
        if (from == 100) {
            from = RULES_TRIGGER_LEVEL - 0.001;
        }
        to = CATEGORY_RULES[i].to * 1;
        if (
                prowizja <= from &&
                prowizja >= to
                ) {
            if (CATEGORY_RULES[i].type == 'notice') {
                notice += CATEGORY_RULES[i].message + '<br>';
            } else if (CATEGORY_RULES[i].type == 'warning') {
                error += CATEGORY_RULES[i].message + '<br>';
            }
        }

    }
    var msg = $(obj).parents('.optionWrapper').find('.message');
    if (notice != '' || error != '') {
        var html = '';
        if (error != '') {
            html += '<span class="error">' + error + '</span>';
        }
        if (notice != '') {
            html += '<span class="notice">' + notice + '</span>';
        }
        $(msg).html(html).show().css({'top': $(obj).parents('.optionWrapper').find('input[type="text"][data-class="row8"]').position().top});
    } else {
        $(msg).hide();
    }
}

var ofertaPrzeliczSumy = function () {
    //sumy
    sumGrouponsCount = 0;
    sumValue = 0;
    sumGrouponGain = 0;

    $('.optionWrapper:visible').each(function () {
        sumGrouponsCount += parseInt($(this).find('input[data-class="row11"]').val() * 1);
        sumValue += $(this).find('input[data-class="row12"]').val() * 1;
        sumGrouponGain += $(this).find('input[data-class="row14"]').val() * 1;
    });

    $('input[data-class="summary1"]').val(sumGrouponsCount);
    $('input[data-class="summary2"]').val(Math.round(sumValue * 100) / 100);

    var row1 = $('input[data-class="summary3"]').val();//procent powr
    var row2 = $('input[data-class="summary4"]').val();//rach powr

    var dodatkowyZysk = (row1 / 100) * sumGrouponsCount * row2;
    if (!isFinite(dodatkowyZysk)) {
        dodatkowyZysk = 0;
    }

    $('input[data-class="summary5"]').val(Math.round((sumValue + dodatkowyZysk) * 100) / 100);
    $('input[data-class="summary6"]').val(sumGrouponGain); //suma dla grp
}

var cloneElement = function () {
    var cloneIndex = $('.clonedElement:not(.cloneBase)').length + 1;

    $(".cloneBase").clone()
            .removeClass('cloneBase')
            .appendTo(".clonedElementsWrapper")
            .find("input, select, textarea")
            .each(function () {
                this.name = this.name.replace('cloneTemplate', cloneIndex);
            });
    $(".removeClonedElement").on("click", removeClonedElement);
}

var removeClonedElement = function () {
    $(this).parents('.clonedElement').remove();
}

var instrumentVariantTypeChange = function () {
    var val = $('.instrumentVariantTypeRadio:checked').val();
    $('.instrumentVariantTypeWrapper').slideUp();
    if (val == 'standard') {
        $('.instrumentVariantTypeStandard').slideDown();
    } else if (val == 'variants') {
        $('.instrumentVariantTypeVariants').slideDown();
    } else if (val == 'cities') {
        $('.instrumentVariantTypeCities').slideDown();
    }
}


/* Polish initialisation for the jQuery UI date picker plugin. */
/* Written by Jacek Wysocki (jacek.wysocki@gmail.com). */
jQuery(function ($) {
    $.datepicker.regional['pl'] = {
        closeText: 'Zamknij',
        prevText: '&#x3c;Poprzedni',
        nextText: 'Następny&#x3e;',
        currentText: 'Dziś',
        monthNames: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec',
            'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
        monthNamesShort: ['Sty', 'Lu', 'Mar', 'Kw', 'Maj', 'Cze',
            'Lip', 'Sie', 'Wrz', 'Pa', 'Lis', 'Gru'],
        dayNames: ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'],
        dayNamesShort: ['Nie', 'Pn', 'Wt', 'Śr', 'Czw', 'Pt', 'So'],
        dayNamesMin: ['N', 'Pn', 'Wt', 'Śr', 'Cz', 'Pt', 'So'],
        weekHeader: 'Tydz',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['pl']);
});

function copyTextToClipboard(text) {
    var textArea = document.createElement("textarea");

    //
    // *** This styling is an extra step which is likely not required. ***
    //
    // Why is it here? To ensure:
    // 1. the element is able to have focus and selection.
    // 2. if element was to flash render it has minimal visual impact.
    // 3. less flakyness with selection and copying which **might** occur if
    //    the textarea element is not visible.
    //
    // The likelihood is the element won't even render, not even a flash,
    // so some of these are just precautions. However in IE the element
    // is visible whilst the popup box asking the user for permission for
    // the web page to copy to the clipboard.
    //

    // Place in top-left corner of screen regardless of scroll position.
    textArea.style.position = 'fixed';
    textArea.style.top = 0;
    textArea.style.left = 0;

    // Ensure it has a small width and height. Setting to 1px / 1em
    // doesn't work as this gives a negative w/h on some browsers.
    textArea.style.width = '2em';
    textArea.style.height = '2em';

    // We don't need padding, reducing the size if it does flash render.
    textArea.style.padding = 0;

    // Clean up any borders.
    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';

    // Avoid flash of white box if rendered for any reason.
    textArea.style.background = 'transparent';


    textArea.value = text;

    document.body.appendChild(textArea);

    textArea.select();

    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copying text command was ' + msg);
    } catch (err) {
        console.log('Oops, unable to copy');
    }

    document.body.removeChild(textArea);
}
