
var _BASEPATH = "/";

CKEDITOR.addStylesSet( 'my_styles',
[
    // Inline Styles 
    //{ name : 'lead', element : 'p', attributes : { 'class' : 'lead' } },
    //{ name : 'obraz z lewej', element : 'img', attributes : { 'class' : 'leftSide' } } 
]);

CKEDITOR.editorConfig = function( config )
{  
    config.stylesCombo_stylesSet = 'my_styles';
    config.ForcePasteAsPlainText = true;
    config.pasteFromWordPromptCleanup = true;
    config.pasteFromWordNumberedHeadingToList = true;
    config.pasteFromWordRemoveFontStyles = true;
    config.pasteFromWordRemoveStyles = true;
    config.startupOutlineBlocks = true;
    config.tabSpaces = 5;
    config.entities_latin = false;
    config.extraAllowedContent = 'iframe(*){*}[*];div(!videodetector)';

    config.filebrowserBrowseUrl = _BASEPATH+'web/admin/js/kcfinder/browse.php?type=files',
    config.filebrowserImageBrowseUrl = _BASEPATH+'web/admin/js/kcfinder/browse.php?type=images',
    config.filebrowserUploadUrl = _BASEPATH+'web/admin/js/kcfinder/upload.php?type=files',
    config.filebrowserImageUploadUrl = _BASEPATH+'web/admin/js/kcfinder/upload.php?type=images'

    config.toolbar = 'MyToolbar';

    config.toolbar_MyToolbar =
    [
        ['Source'],
        ['Cut','Copy','Paste','PasteText','-'],
        ['Undo','Redo','-','RemoveFormat'],
        ['Image','SpecialChar','VideoDetector'],
        '/',
        ['Format', 'Styles'], //'Styles',
        ['Bold','Italic','Strike','-','Subscript','Superscript'],
        ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
        ['NumberedList','BulletedList','-','Outdent','Indent'],
        ['Link','Unlink','Anchor'],
        ['Maximize']
    ];

    config.format_tags = 'h1;h3;h4;h5;h6;p';

    config.contentsCss = _BASEPATH+'web/admin/css/ckeEditor.css';
    config.extraPlugins = 'justify,nbsp,videodetector';
};
