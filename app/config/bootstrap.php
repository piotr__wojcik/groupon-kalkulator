<?php
//do not use framework tools here (besides config) - it's to early
//all proper config files have to be in configs/ subdirectory

//load the proper config handler (we dont' have any autoload yet)
require_once('lib/core/configLoaders/PHPArrayConfigLoader.class.php');

//set loader - this will load the default config file
use lib\core\configLoaders\PHPArrayConfigLoader;
Config::setLoader(new PHPArrayConfigLoader());

//==============================================================================
//this is were we change configuration depending on domain, and other stuff

ini_set('display_errors',0);
date_default_timezone_set("Europe/Warsaw");

//load app specific functions
require_once('app/functions.php');

if (preg_match('/kalkulator-groupon\.local55\.pl/', $_SERVER['SERVER_NAME'])) {
   error_reporting(E_ALL & ~(E_NOTICE));
   ini_set('display_errors',1);   
   Config::read('dev');
}
if (preg_match('/kalkulator\.groupon\.bidev\.pl/', $_SERVER['SERVER_NAME'])) {
   ini_set('display_errors',0);   
   Config::read('prod');
}

//==============================================================================
//beyond this point the framework will start full initialization
//any changes to the config should be made above
