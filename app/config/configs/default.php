<?php

$__CONFIG = array(
    //these are essential for the framework init
    'APP_NAME' => 'framework', //this should be the same for the whole config
    'INIT_HANDLER'=>'baseInitHandler',
    'REQUEST_CLEANER'=>'baseRequestCleaner',
    'PROD_SERVER'=>false, //to know whether we are on prod or on dev (two values for easier user)
    'TEST_SERVER'=>true,
    
    //these are needed for baseInitHandler and it's possible children
    'USE_SESSION'=>true,
    'SESSION_HANDLER'=>'lib\core\sessionHandlers\PHPSessionHandler',
    'USE_CACHE'=>true,
    'CACHE_HANDLER'=>'lib\core\cacheHandlers\FileCache',
    'CONTEXT_DETECTOR'=>'app\core\contextDetector',
    'ROUTER_HANDLER'=>'app\core\routerHandler',
    
    //db
    'DB_HOST' => '',
    'DB_USER' => '',
    'DB_PASS' => '',
    'DB_NAME' => '',
    'DB_PORT' => 3306,
    
    //hash salt
    'HASH_SALT'=> 'insertvaluehere',
    
    //where are the templates located
    'TEMPLATES_DIR'=>'web/admin/templates/',
    'USER_TEMPLATES_DIR'=>'web/user/templates/',
    'PDF_TEMPLATE'=>'web/user/templates/pdf.htm',
    
    //page specific
    'GOOGLE_OAUTH_CLIENT_ID' => '',
    'GOOGLE_OAUTH_CLIENT_SECRET' => '',
    'GOOGLE_OAUTH_REDIRECT_URI' => '',
);
