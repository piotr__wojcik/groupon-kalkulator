<?php

namespace app\core;

use IRouterHandler;
use app\user;
use \app\helpers\Mobile_Detect;
    
class routerHandler implements IRouterHandler {

   private $controller = NULL;
   private $method = NULL;

   public function translateRequest() {
      if ($_GET['URL']!="") {
         $params = explode("/",$_GET['URL']);
         $numberOfParams = count($params);
         $counter = 0;
         
         if ($numberOfParams>0) {
            
            for ($i=0; $i<$numberOfParams; $i++) {
               $param = strtolower($params[$i]);
               if ( (preg_match('/^pageNr-(?P<nrStrony>\d+)$/',$params[$i], $matches)) == 1) {
                  \Request::get()->pageNr = $matches['nrStrony'];
               }
               //zmienna:wartość
               else if ( (preg_match('/(?P<zmienna>(.+)):(?P<wartosc>.+)/',$params[$i], $matches)) == 1) {
                  \Request::get()->$matches['zmienna'] = $matches['wartosc'];
               }
               //zwykły napis, czyli kolejny param
               else {
                
                  //modul
                  if ($counter==0) {
                     $this->controller = $params[$i];
                  }
                  //metoda
                  else if ($counter==1) {
                     $this->method = $params[$i];
                     $paramName = "param{$counter}";
                     \Request::get()->$paramName = $params[$i];
                  }
                  //parametr
                  else {
                     $paramName = "param{$counter}";
                     \Request::get()->$paramName = $params[$i];
                  }
                  $counter++;
               }
            }
            
         }
      }
      
   }
   
   public function beforeExecute() {
      if ($_SESSION['log'] != '') {
         \Log::readSave($_SESSION['log']);
         $_SESSION['log'] = NULL;
      }
   }
   
   public function doExecute() {
      $controllerName = $this->controller."Controller";
      $controller = new $controllerName;
      $methodName = $this->method;
      
      if (!method_exists($controller, $methodName)) {
         $methodName = $controller->defaultMethod;
      }
      $controller->beforeExecute();
      $ret = $controller->$methodName();
      $controller->afterExecute();
      
      return $ret;
   }
   
   public function afterExecute() {
   
   }
   
}
