<?php

namespace app\core\delayedCommand;

class DelayedCommand extends \Model
{
    
    //status maxlength = 10
    const STATUS_PLANNED = "planned";
    const STATUS_EXECUTING = "executing";
    const STATUS_EXECUTED = "executed";
    const STATUS_ERROR = "error";

    public function __construct($id = null) 
    {
        $this->id = (int)$id;
        $this->baza = \BazaDanych::dajBaze();
        $this->tabela = "delayed_events";
    }
    
    public function plan(IPlannableCommand $command, $executionDate = null) 
    {
        $commandData = $command->getCommandData();
        if (isset($commandData['priority'])) {
            $priority = $commandData['priority'];
        } else {
            $priority = 9;
        }
        
        $addData = array(
            'command_data' => serialize($commandData),
            'priority' => $priority,
            'command_class' => get_class($command),
            'execute_on' => !empty($executionDate) ? $executionDate : date("Y-m-d H:i:s"),
            'created' => date("Y-m-d H:i:s"),
            'status' => self::STATUS_PLANNED,
        );
        $this->add($addData);
    }
    
    public function execute() 
    {
        $startTime = $this->time();
        //mark start of execution
        $this->baza->sql("
            update
                {$this->tabela}
            set
                status = '" . self::STATUS_EXECUTING ."',
                last_change = '" . date("Y-m-d H:i:s") . "',
                tries = tries+1
            where
                id = '{$this->id}'
        ");
        
        //prepare object
        $this->getData(true);
        $obj_class = $this->rawData['command_class'];
        $obj = new $obj_class();
        $res = $obj->executeCommand(unserialize($this->data['command_data']));
        
        //execution data
        $used_memory = $this->memory_usage();
        $execution_time = round($this->time()-$startTime,6)."s";
        $info = null;
        if (is_array($res)) {
            $info = $res[1];
            $res = $res[0];
        }
        
        //update db row
        $updateData = array(
            'last_change' => date("Y-m-d H:i:s"),
            'status' => $res===true ? self::STATUS_EXECUTED : self::STATUS_ERROR,
            'info' => $info,
            'execution_time' => $execution_time,
            'used_memory' => $used_memory,
        );
        $this->update($updateData);
    }
    
    private function time() 
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }
    
    private function memory_usage()
    {
        $size = memory_get_usage(true);
        $unit=array('B','KB','MB','GB','TB','PB');
        return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
    }
    
    //==========================================================================
    //static context
    
    public static function executePlannedCommands($timeLimit = null, $worker = null, $jobsLimit = null) 
    { 
        //default values
        $timeLimit = (int)$timeLimit;
        if ($timeLimit<1) {
            $timeLimit = 60;
        }
        $worker = (int)$worker;
        if ($worker<1) {
            $worker = 1;
        }
        $jobsLimit = (int)$jobsLimit;
        if ($jobsLimit<1) {
            $jobsLimit = 30;
        }
       
        $m = new self();
        $startTime = $m->time();
      
        //we are looking for commands that have a execute_on time <= current time 
        $result = $m->baza->sql("
            select 
                id
            from 
                {$m->tabela}
            where
                status = '" . self::STATUS_PLANNED . "'
                and execute_on <= '" . date("Y-m-d H:i:s") . "'
            order by 
                priority asc
            limit " . (($worker-1)*$jobsLimit) . ", {$jobsLimit}
        ");
        while ($row = fetch_assoc($result)) { 
            $m = new self($row['id']);
            //5 seconds safety buffer
            if ($m->time()-$startTime > $timeLimit-5) {
                die('Out of time');
            } else {
                $m->execute();
            }
        }
        die('All done in ' . round($m->time()-$startTime,6) . 's');
    } 
}
