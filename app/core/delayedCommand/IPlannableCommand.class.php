<?php

namespace app\core\delayedCommand;

interface IPlannableCommand 
{
   
    public function executeCommand($data); //should return boolean
       
    public function getCommandData();  
}
