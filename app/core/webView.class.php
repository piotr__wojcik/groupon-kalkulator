<?php

namespace app\core;

class webView {

   protected $file;
   protected $data;
   protected $additionalDataRead = false;
   protected $additionalData = array();
   protected $templates_dir = NULL;
   
   public function __construct($data, $file, $templates_dir = NULL) {
      $this->data = $data;
      if ($templates_dir == NULL) {
         $this->templates_dir = \Config::get()->TEMPLATES_DIR;
      }
      else {
         $this->templates_dir = $templates_dir;
      }
      if ($file!='')
         $this->file = $this->templates_dir.$file;
   }
   
   public function show($noWrap = FALSE) {
      $template = $this->getTemplateContent($this->data, $this->file);
      if ($noWrap) {
         return $template;
      }
      $adminModel = \Admin::dajModel();
      $data = array(
         'content'=>$template,
         'templateData' => $this->data,
         'userCanChangePassword' => $adminModel!==false ? $adminModel->canChangePassword() : false,
      );
      $templateMain = $this->getTemplateContent($data, $this->templates_dir.'index.htm');
      return $templateMain;
   }
   
   protected function readAdditionalData() {
      $this->additionalData = array(   
      
      );
		
      $this->additionalDataRead = true;
   }
	
   //===========================================================================
   //PODSTAWOWE FUNKCJE
   
   protected function getTemplateContent($data, $template) {
      if (!$this->additionalDataRead) {
         $this->readAdditionalData();
      }
      $data = array_merge($data, $this->additionalData);
      extract($data);
		
      if (!file_exists($template)) {
         $trace = debug_backtrace();
         trigger_error('View - nie ma takiego pliku template: ' . $template .' in ' . $trace[0]['file'] .' on line ' . $trace[0]['line'],E_USER_WARNING);
         return FALSE;
      }
         
      ob_start();
      include($template);
      $msg = ob_get_contents();
      ob_end_clean();
      return $msg;
   }
   
}
