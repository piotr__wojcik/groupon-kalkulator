<?php

use app\core\webView;

class adminController extends baseController {

   public function beforeExecute() {
      if (get_class($this) !== "adminController" && !Admin::zalogowany()) {
         redirect('/admin');
      }
   }

   public function show() {
      if (!Admin::zalogowany()) {
         $client = new Google_Client();
         $client->setClientId(\Config::get()->GOOGLE_OAUTH_CLIENT_ID);
         $client->setClientSecret(\Config::get()->GOOGLE_OAUTH_CLIENT_SECRET);
         $client->setRedirectUri(\Config::get()->GOOGLE_OAUTH_REDIRECT_URI);
         $client->addScope("email");
         $service = new Google_Service_Oauth2($client);
         $authUrl = $client->createAuthUrl();
         
         $viewData = array(
            'login' => \Request::get()->raw->post['Login']['login'],
            'googleAuthUrl' => $authUrl,
         );
         $view = new webView($viewData, 'login.htm');
      }
      else {
         redirect('/offers');
         
         //nieuzywane
         $viewData = array();
         $view = new webView($viewData, 'mainpage.htm');
      }
      
      return $view->show();
   }
   
   public function logout() {
      Admin::logout();
      redirect('/admin');
   }
   
}
