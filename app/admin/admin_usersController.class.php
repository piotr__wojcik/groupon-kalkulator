<?php

use app\core\webView;
use app\admin\admin_user;

class admin_usersController extends adminController {
   
   const PRV_EDIT = "admin_users_edit";

   public function show() {
      if (!Admin::czyMoze(self::PRV_EDIT)) {
         redirect('/admin');
      }
      
      $model = new admin_user();
      $list = $model->list_for_admin(1, 100000, Session::get()->admin['search'][get_class($this)]);
      
      $viewData = array(
         'list'=>$list,
         'search'=>Session::get()->admin['search'][get_class($this)],
      );
      $view = new webView($viewData, 'admin_users/list.htm');
      
      return $view->show();
   }
   
    public function search() 
    {
        Session::get()->admin['search'][get_class($this)] = Request::get()->raw->post['Search'];
        redirect('/admin_users');
    }
   
    public function search_reset() 
    {
        Session::get()->admin['search'][get_class($this)] = array();
        redirect('/admin_users');
    }
   
   public function delete() {
      if (!Admin::czyMoze(self::PRV_EDIT)) {
         redirect('/admin');
      }
      
      $model = new admin_user(Request::get()->id);
      if ($model->deleteable()) {
         $model->delete();
      }
      
      redirect('/admin_users');
   }
   
   public function edit() {
      if (!Admin::czyMoze(self::PRV_EDIT)) {
         redirect('/admin');
      }
      
      $model = new admin_user(Request::get()->id);
      if (!$model->exists()) {
         \Log::error("Administrator nie istnieje");
         redirect('/admin_users');
      }
      
      if (isset(Request::get()->raw->post['Edit'])) {
         if ($model->update(Request::get()->raw->post['Edit'])) {
            Log::confirm("Zmiany zostały zapisane");
            redirect('/admin_users/edit/id:'.Request::get()->id);
         }
      }
      $model->getData(true);
      $viewData = array(
         'model'=>$model,
         'possible_superiors' => $model->getPossibleSuperiors(),
      );
      $view = new webView($viewData, 'admin_users/change.htm');
      
      return $view->show();
   }
   
   public function edit_self() {
      $model = new admin_user(Admin::dajId());
      if (!$model->exists()) {
         \Log::error("Administrator nie istnieje");
         redirect('/admin');
      }
      
      if (isset(Request::get()->raw->post['Edit'])) {
         if ($model->updateSelf(Request::get()->raw->post['Edit'])) {
            Log::confirm("Zmiany zostały zapisane");
            redirect('/admin_users/edit_self');
         }
      }
      $model->getData(true);
      $viewData = array(
         'model'=>$model,
      );
      $view = new webView($viewData, 'admin_users/change-self.htm');
      
      return $view->show();
   }
   
   
   public function add() {
      if (!Admin::czyMoze(self::PRV_EDIT)) {
         redirect('/admin');
      }
      
      $model = new admin_user();
      
      if (isset(Request::get()->raw->post['Edit'])) {
         if ($model->add(Request::get()->raw->post['Edit'])) {
            Log::confirm("Administrator został dodany");
            redirect('/admin_users/edit/id:'.$model->id);
         }
      }
      
      $viewData = array(
         'model'=>$model,
      );
      $view = new webView($viewData, 'admin_users/change.htm');
      
      return $view->show();
   }
}
