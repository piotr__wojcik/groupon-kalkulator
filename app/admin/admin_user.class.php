<?php

namespace app\admin;

class admin_user extends \Model {
   private $tabelaUpr = "admins_privileges";

   public function __construct($id = -1) {
      $this->baza = \BazaDanych::dajBaze();
      $this->tabela = "admins";
      $this->id = $id;
   }
   
    public function login_with_password($login, $haslo) 
    {
        $row = fetch_assoc($this->baza->sql("
            select 
                id, login, password 
            from 
                {$this->tabela} 
            where 
                login='" . escape_string($login) . "' 
                and password='" . escape_string($haslo) . "'
                and password<>''
                and google_only_login=0
         "));
        /*
        $row = fetch_assoc($this->baza->sql("
            select id, login, password from {$this->tabela} where login='" . escape_string($login) . "' 
            "));
        $haslo = $row['password'];
        */
        if ($row['id']!="" && $login == $row['login'] && $haslo==$row['password'] ) {
            $this->id = $row['id'];
            $this->getData(true);
            $this->baza->sql("update {$this->tabela} set lastLogin='".date("Y-m-d H:i:s")."' where id='{$this->id}'");
            return true;
        }
        else
            return false;
         
    }
    
    public function login_with_google($email)
    {
        $row = fetch_assoc($this->baza->sql("
            select 
                id
            from 
                {$this->tabela} 
            where 
                login='" . escape_string($email) . "' 
         "));

        if (!empty($row['id'])) {
            $this->id = $row['id'];
            $this->getData(true);
            $this->baza->sql("update {$this->tabela} set lastLogin='".date("Y-m-d H:i:s")."' where id='{$this->id}'");
            return true;
        }
        else
            return false;
    }
   
   public function initZLoginu($login) {
      $row = fetch_assoc($this->baza->sql("select id from {$this->tabela} where login='{$login}'"));
      $this->id = $row['id'];
   }
   
    public function list_for_admin($page, $onPage, $params = array()) 
    {
        cleanData($params);
        $sql = "
            select 
                * 
            from 
                {$this->tabela}
            where
                deleteable <> 0
                
                " . (!empty($params['login']) ? "
                and login like '%{$params['login']}%'
                " : "") . "
                
            order by 
                login asc
        ";
        return new \Lista($this, $sql, $page, $onPage);
    }
   
   public function deleteable() {
      $this->getData(true);
      return $this->data['deleteable'] == 1;
   }
   
   protected function afterRead() {
      $this->data['privileges'] = array();
      $wynik = $this->baza->sql("select privilege_id from {$this->tabelaUpr} where admin_id='{$this->id}'");
      
      while ($row = fetch_assoc($wynik)) {
         $this->data['privileges'][] = $row['privilege_id'];
      }
   }
   
   public function checkData(&$dane, $update = false) {
      cleanData($dane);
      if ($dane['login']=="") {
         \Log::error("Należy podać login");
         return false;
      }
      else if (!$update) {
         $check = fetch_assoc($this->baza->sql("
            select 
               id 
            from 
               {$this->tabela}
            where
               login='{$dane['login']}'
            "));
         if ($check['id']!="") {  
            \Log::error("{$dane['login']} - Administrator z takim loginem już istnieje");
            return false;
         }
      }
      
      if ($dane['google_only_login']==0) {
          if (!$update) {
             if ($dane['password']=="") {
                \Log::error("Należy podać hasło");
                return false;
             }
             if (mb_strlen($dane['password'],'utf-8')<8) {
                \Log::error("Hasło powinno mieć minimum 8 znaków");
                return false;
             }
             if ($dane['password']!=$dane['retype_password']) {
                \Log::error("Hasło zostało źle powtórzone");
                return false;
             }
             unset($dane['retype_password']);
             $dane['password'] = _hash($dane['password']);
          }
          else {
             if ($dane['password']!="") {
                if (mb_strlen($dane['password'],'utf-8')<8) {
                   \Log::error("Hasło powinno mieć minimum 8 znaków");
                   return false;
                }
                if ($dane['password']!=$dane['retype_password']) {
                   \Log::error("Hasło zostało źle powtórzone");
                   return false;
                }
                unset($dane['retype_password']);
                $dane['password'] = _hash($dane['password']);
             }
             else {
                unset($dane['password'], $dane['retype_password']);
             }
          }
      }
      unset($dane['retype_password']);
      
      if (!$update) {
         $dane['created'] = date("Y-m-d H:i:s");
      }
      $dane['lastChange'] = date("Y-m-d H:i:s");
      
      return true;
   }
   
   protected function afterAdd(&$dane) {
      $this->managePrivileges($dane['privileges']);
   }
   
   protected function afterUpdate(&$dane) {
      $this->managePrivileges($dane['privileges']);
   }
   
   protected function afterDelete() {
      $this->baza->sql("
         delete 
            from 
               {$this->tabelaUpr}
            where
               admin_id = '{$this->id}'
         ");
   }
   
   private function managePrivileges($dane) {
      if (!is_array($dane)) {
         $dane = array();
      }
      
      $this->getData(true);
      
      if (!$this->deleteable()) {
         return;
      }
      
      foreach ($dane as $privilege_id) {
         if (!in_array($privilege_id, $this->data['privileges'])) {
            $this->baza->sql("
               insert
                  into 
                     {$this->tabelaUpr}
                     (admin_id, privilege_id)
                  values
                     ('{$this->id}','{$privilege_id}')
            ");
         }
      }
      
      foreach ($this->data['privileges'] as $privilege_id) {
         if (!in_array($privilege_id, $dane)) {
            $this->baza->sql("
               delete
                  from 
                     {$this->tabelaUpr}
                  where
                     admin_id = '{$this->id}'
                     and privilege_id= '{$privilege_id}'
            ");
         }
      }
   } 
   
   public function updateSelf($dane) {
      $this->getData(true);
      
      if (_hash($dane['current_password']) != $this->data['password']) {
         \Log::error("Aktualne hasło zostało błędnie podane");
         return false;
      }
      
      if ($dane['password']=="") {
         \Log::error("Należy podać nowe hasło");
         return false;
      }
      if (mb_strlen($dane['password'],'utf-8')<8) {
         \Log::error("Nowe hasło powinno mieć minimum 8 znaków");
         return false;
      }
      if ($dane['password']!=$dane['retype_password']) {
         \Log::error("Nowe hasło zostało źle powtórzone");
         return false;
      }
      $dane['password'] = _hash($dane['password']);
      $this->baza->sql("
         update 
            {$this->tabela} 
         set 
            password='{$dane['password']}' 
         where 
            id='{$this->id}'
         ");
      return true;
   }
   
    public function canChangePassword()
    {
        $this->getData(true);
        return $this->data['google_only_login']==0;
    }
   
    public function getPossibleSuperiors()
    {
        $sql = "
            select
                id, login, name
            from
                {$this->tabela}
            where
                id <> '{$this->id}'
                and deleteable = 1
            order by
                login asc
        ";
        $result = $this->baza->sql($sql);
        $ret = array();
        while ($row = fetch_assoc($result)) {
            $ret[$row['id']] = "{$row['login']}" . (!empty($row['name']) ? " ({$row['name']})" : "");
        }
        
        return $ret;
    }
    
    public function getMinions()
    {
        $minions = array();
        
        $alreadyChecked = array();
        $checkList = array($this->id);
        
        while (!empty($checkList)) {
            $superiorId = array_shift($checkList);
            $alreadyChecked[] = $superiorId;
            $sql = "
                select
                    id
                from
                    {$this->tabela}
                where
                    superior = '{$superiorId}'
            ";
            $result = $this->baza->sql($sql);
            while ($row = fetch_assoc($result)) {
                if (!in_array($row['id'], $minions)) {
                    $minions[] = $row['id'];
                }
                if (!in_array($row['id'], $alreadyChecked)) {
                    $checkList[] = $row['id'];
                }
            }
        }
        
        return $minions;
    }
}

