<?php

use \Admin;

class googleLoginController extends baseController 
{

    public function callback()
    {
        if (Admin::logInWithGoogle($_GET['code'])) {
            redirect("/admin");
        } else {
            Log::error("Wydaje się, że Twoje konto nie ma uprawnień do zalogowania się do kalkulatora");
            redirect("/admin");
        }
    }
}
