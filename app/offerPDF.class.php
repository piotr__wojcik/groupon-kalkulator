<?php

class offerPDF extends TCPDF {

    public function __construct() {
      
        parent::__construct(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
      
        $this->light = $this->addTTFfont('app/tcpdf-6/fonts/opensanslight.ttf', 'TrueTypeUnicode', '', 32);
        $this->regular = $this->addTTFfont('app/tcpdf-6/fonts/opensansregular.ttf', 'TrueTypeUnicode', '', 32);
        $this->bold = $this->addTTFfont('app/tcpdf-6/fonts/opensanssemibold.ttf', 'TrueTypeUnicode', '', 32);
      
      
        // set document information
        $this->SetCreator(PDF_CREATOR);
        $this->SetAuthor('Groupon');
        //marginesy
        $this->SetMargins(35, 35);
        $this->setImageScale(PDF_IMAGE_SCALE_RATIO);
      
        //set auto page breaks
        $this->SetAutoPageBreak(true, 45);
        // set font
        $this->SetFont($this->regular, '', 7.5);
      
        $this->AddPage();
        
    }
    
    public function generate($data)
    {
        //$this->writeHTML($this->_styles($data), true, false, true, false, '');
        $this->writeHTML($this->_header($data), true, false, true, false, '');
        $this->writeHTML($this->_offer($data), true, false, true, false, '');
        
        //return string
        return $this->Output($nazwaPliku, 'S');
        //output directly
        //$this->Output($nazwaPliku, 'I');
      
        //write file
        //$this->Output($nazwaPliku, 'F');
    } 
    
    //==========================================================================
    
    private function _offer($data)
    {
        extract($data);
        $styles = array(
            'border' => 'border:1px solid #dddddd;',
            'border-top' => 'border-top:1px solid #dddddd;',
            'border-left' => 'border-left:1px solid #dddddd;',
            'background-green' => 'background-color: #53a318;color:white;font-family:'.$this->bold.';',
            'background-grey' => 'background-color: #f9f9f9;',
            'text-center' => 'text-align:center;',
            'text-right' => 'text-align:right;',
            'font-bold' => 'font-family:'.$this->bold.';',
            'faqHeader' => 'color: #333333;font-weight:normal;padding-bottom: 9px;margin: 0 0 30px;border-bottom: 1px solid #eeeeee;',
        );
        
        $html = '';
        
         
        if (!empty($model->data['comment'])) {
            $html .= '
        <p style="margin-bottom:30px">' . nl2br($model->data['comment']) . '</p>
                ';
        }
 
    
        //open table
        $html .= '
        <table border="0" cellspacing="0" cellpadding="0">
        ';
        for ($i = 1; $i<=$model->data['no_of_variants']; $i++) {
            //open a row
            if ($i % 3 == 1) {
                $html .= '
            <tr>
                ';
            }
            //write column
            $html .= '
                <td width="33%" valign="top">
                    <table border="0" cellspacing="0" cellpadding="7" style="' . $styles['border'] . '" width="98%">
                        <tr>
                		    <th colspan="2" style="' . $styles['border'].$styles['background-green'].$styles['text-center'] . '">PAKIET ' . $i . '</th>
                	    </tr>
                        <tr>
                            <td width="55%" style="' . $styles['border'].$styles['background-grey'] . '">Przelew dla Partnera</td>
                            <td width="45%" style="' . $styles['border'].$styles['background-grey'] . '">' . $model->data['variant'.$i]['summary']['partnerGain'] . ' zł</td>
                        </tr>
                        <tr>
                            <td style="' . $styles['border'] . '">Zasięg kampanii</td>
                            <td style="' . $styles['border'] . '">' . $model->data['variant'.$i]['summary']['instrumentsRange'] . ' u.u.</td>
                        </tr>
                        <tr>
                            <td style="' . $styles['border'].$styles['background-grey'] . '">Instrumenty</td>
                            <td style="' . $styles['border'].$styles['background-grey'] . '">' . $model->data['variant'.$i]['summary']['instrumentsValue'] . ' zł</td>
                        </tr>
                        <tr>
                            <td style="' . $styles['border'] . '">Powracający klienci</td>
                            <td style="' . $styles['border'] . '">' . $model->data['variant'.$i]['summary']['returningValue'] . ' zł</td>
                        </tr>
                        <tr>
                            <td style="' . $styles['border'].$styles['background-green'] . '">WARTOŚĆ KAMPANII</td>
                            <td style="' . $styles['border'].$styles['background-green'] . '">' . format_value($model->data['variant'.$i]['summary']['totalSum']) . ' zł</td>
                        </tr>
            ';
            if ($model->data['summary_settings']['valueForCoupon']==1) {
                $html .= '
                        <tr>
                            <td style="' . $styles['border'] . ';font-size:6.5pt;">Wart. kampanii na kupon</td>
                            <td style="' . $styles['border'] . '">' . @format_value($model->data['variant'.$i]['summary']['totalSum']/(int)$model->data['variant'.$i]['summary']['grouponsCount']) . ' zł</td>
                        </tr>
                ';
            }
            
            //options
            if ($model->data['summary_settings']['showOptions']==1) {
                $html .='
                        <tr>
                            <td colspan="2" style="' . $styles['border'].$styles['text-center'].$styles['font-bold'] . '">OPCJE</td>
                        </tr>
                ';
                for ($j=1; $j<11; $j++) { 
                    $daneOpcji = $model->data['variant'.$i][$j];
                    if ($daneOpcji['use']!=1) {
                        continue;
                    }
                    
                    $html .= '
                        <tr>
                            <td colspan="2" style="' . $styles['border'].$styles['text-center'].$styles['background-grey'] . '">' . $daneOpcji['name'] . '</td>
                        </tr>
                    ';
                    if ($model->data['summary_settings']['normalPrice']==1) {
                        $html .= '
                        <tr>
                            <td style="' . $styles['border'].$styles['text-right']. '">Normalna cena</td>
                            <td style="' . $styles['border'].$styles['text-center']. '">' . format_value($daneOpcji[normalPrice]) . ' zł</td>
                        </tr>
                        ';
                    }
                    
                    if ($model->data['summary_settings']['price']==1) {
                        $html .= '
                        <tr>
                            <td style="' . $styles['border'].$styles['text-right']. '">Oferta Groupon</td>
                            <td style="' . $styles['border'].$styles['text-center']. '">' . format_value($daneOpcji[price]) . ' zł</td>
                        </tr>
                        ';
                    }
                    
                    if ($model->data['summary_settings']['discount']==1) {
                        $html .= '
                        <tr>
                            <td style="' . $styles['border'].$styles['text-right']. '">Rabat</td>
                            <td style="' . $styles['border'].$styles['text-center']. '">' . format_value($daneOpcji[discount]) . '%</td>
                        </tr>
                        ';
                    }
                    
                    if ($model->data['summary_settings']['commission']==1) {
                        $html .= '
                        <tr>
                            <td style="' . $styles['border'].$styles['text-right']. '">Prowizja Groupon</td>
                            <td style="' . $styles['border'].$styles['text-center']. '">' . format_value($daneOpcji[commission]) . '%</td>
                        </tr>
                        ';
                    }
                    
                    if ($model->data['summary_settings']['partnerGain']==1) {
                        $html .= '
                        <tr>
                            <td style="' . $styles['border'].$styles['text-right']. '">Przelew dla partnera</td>
                            <td style="' . $styles['border'].$styles['text-center']. '">' . format_value($daneOpcji[partnerGain]) . ' zł</td>
                        </tr>
                        ';
                    }
                    
                    if ($model->data['summary_settings']['grouponsCount']==1) {
                        $html .= '
                        <tr>
                            <td style="' . $styles['border'].$styles['text-right']. '">Ilość grouponów</td>
                            <td style="' . $styles['border'].$styles['text-center']. '">' . format_value($daneOpcji[grouponsCount]) . '</td>
                        </tr>
                        ';
                    }
                    
                    if ($model->data['summary_settings']['sumPartnerGain']==1) {
                        $html .= '
                        <tr>
                            <td style="' . $styles['border'].$styles['text-right']. '">&Sigma; przelewu dla opcji</td>
                            <td style="' . $styles['border'].$styles['text-center']. '">' . format_value($daneOpcji[sumPartnerGain]) . ' zł</td>
                        </tr>
                        ';
                    }
                }
            }
            
            $html .= '
                        <tr>
                		    <th colspan="2" style="' . $styles['border'].$styles['background-green'].$styles['text-center'] . '">SKŁAD PAKIETU</th>
                	    </tr>
                        <tr>
                            <td colspan="2" style="line-height:18px">' . implode('<br />', $model->data['variant'.$i]['summary']['instrumentNames']) . '</td>
                        </tr>
                    </table>
                    <br /><br />
                </td>
            ';
            //close row
            if ($i % 3 == 0) {
                $html .= '
            </tr>
                ';
            }
        }
        //fill the rest of cells if needed
        if ($i % 3 != 1) {
            $html .= '
                <td colspan="' . ($i % 3 == 2 ? "2" : "1") . '"></td>
            </tr>
            ';
        }
        //close table
        $html .= '
        </table>
        
        ';
        
        //list instruments
        $html .= '<br><br>';
        foreach ($model->data['usedInstruments'] as $instrument) {
            if (!$instrument->hasFAQ()) {
                continue;
            }
            $faqContent = preg_replace('/<h[1-6]>/','<p style="'.$styles['font-bold'].'">', $instrument->data['faq']);
            $faqContent = preg_replace('/<\/h[1-6]>/','</p>', $faqContent);
            
            $html .= '
        <h4 style="margin-top:50px;' . $styles['faqHeader'] . '">' . $instrument->data['name'] . '</h4>
        <div>
            '. strip_tags($faqContent, '<br><b><i><u><a><strong><ul><ol><li><p>') . '
        </div>
                ';
        }
        
        return $html;
    }
    
    private function _header($data)
    {
        extract($data);
        
        $html = '
        <img src="/web/admin/images/logo.png">
        <div style="color:black;font-size:30px;line-height:40px;height:30px;">Oferta dla ' . $model->data['name'] . '</div>
        <br /><br />
        ';
        
        return $html;
    }
    
    private function _styles($data)
    {
        $html = '
        <style>

            body { 
                line-height: 20px;
            }
            
            h1, h2, h3, h4, h5, h6, table, tr, td, p, ul, li { 
                margin: 0; padding: 0;
            }
            
            p { 
                color: #8a8a8a; font-size: 10pt; margin: 2mm 0 0 0; 
            }
            
            table.main {
                
                width:100%;
            }
            
            table.table { 
                border-spacing: 0;
                
                width:100%;
                
                border-collapse: separate;
                border-color: #dddddd #dddddd #dddddd #dddddd;
                border-image: none;
                border-radius: 4px;
                border-style: solid solid solid solid;
                border-width: 1px 1px 1px 1px;
                margin-right:10px;
                
            }
            
            table.table th, table.table td {
                border-top: 1px solid #dddddd;
                line-height: 15px;
                padding: 8px;
                text-align: left;
                vertical-align: middle;
            }
            
            table.table td.smaller {
                font-size:10px;
            }
            
            table.table th.noTopBorder, table.table td.noTopBorder {
                border-top-width:0;
            }
            
            table.table th.border-left, table.table td.border-left {
                border-left: 1px solid #dddddd;
                width:45%;
            }
            
            tr.grey td, tr.grey th {
                background-color: #f9f9f9;
            }
            
            table td.green,
            table th.green {
                background-color: #53a318;
                color:white;
                font-weight:bold;
            }
            
            table.table .optionsHeaderCell {
                text-align:center;
                font-weight:bold;
            }
            
            table.table .optionsNameCell {
                /*border-top: 3px solid #dddddd;*/
                background: #f9f9f9;
            }
            
            table.table .textright {
                text-align:right;
            }
            
            table.table .textcenter {
                text-align:center;
            }
            
            table.table .paddingsmall {
                padding:3px;
            }
        
            .page-header
            {
                display:none;    
            }
            
            #logo {
                background:none
                padding-left:0;
                
            	display:block;
                color:black;
                font-size:30px;
                line-height:30px;
                height:30px;
                letter-spacing:-0.02em;
                text-decoration:none;
                margin-bottom:20px;
            }
            
            #logo .print-only {
                color: #53a318;
                display:inline;
                font-weight:bold;
            }
            
            .clear-fix {
                clear: both;
                float: none;
                font-size: 1px;
                height: 0;
                line-height: 1px;
                overflow: hidden;
                width: 98%;
            }
            
            td.main {
                width:33%;
                
            }
            
            .faqHeader {
                color: #333333;
                font-weight:normal;
                padding-bottom: 9px;
                margin: 0 0 30px;
                border-bottom: 1px solid #eeeeee;
            }
            
        </style>
        ';
        
        return $html;
    }
    
    public function Header() 
    {
        return "";
    }
    
    public function Footer() 
    {
        return "";
    }
}
