<?php

use \app\core\webView;
use \app\offers\Offer;
//use \Admin;
use \app\settings\Categories\Category;
use \app\settings\Cities\City;
use \app\settings\Instruments\Instrument;

class OffersController extends adminController 
{
    
    const PRV_EDIT_ALL = "offers_edit_all";
    
    protected $url = "/offers";
   
    public function getModel($id = null)
    {
        $model = new Offer($id);
        $model->getData(true);
        if (
            !empty($id) && 
            (
                $model->data['user_id']!=Admin::dajId() &&
                !in_array($model->data['user_id'], Admin::getMinions())
            ) &&
            !Admin::czyMoze(self::PRV_EDIT_ALL)
        ) {
            redirect($this->url);
        }
        return $model;
    } 
   
    public function show() 
    {
        $model = $this->getModel();
        
        $minions = Admin::getMinions();
        if (!Admin::czyMoze(self::PRV_EDIT_ALL)) {
            if (empty($minions)) {
                Session::get()->admin['search'][get_class($this)]['user_id'] = Admin::dajId();
            } else {
                Session::get()->admin['search'][get_class($this)]['user_id'] = $minions;
                Session::get()->admin['search'][get_class($this)]['user_id'][] = Admin::dajId();
            }
        } else {
            Session::get()->admin['search'][get_class($this)]['user_id'] = null;
        }   
        
        $list = $model->list_all(
            Session::get()->admin['search'][get_class($this)], 
            \Request::get()->get['pageNr'], 
            20
        );
      
        $viewData = array(
            'list'=>$list,
            'search'=>Session::get()->admin['search'][get_class($this)],
            'page' => \Request::get()->get['pageNr'],
            'pages' => paginator($list->data['page'], $list->data['pages'], $this->url),
            'host' => "http://".$_SERVER['HTTP_HOST'],
            'showOfferOwner' => Admin::czyMoze(self::PRV_EDIT_ALL) || !empty($minions)
        );
        $view = new webView($viewData, 'offers/list.htm');
      
        return $view->show();
    }
   
    public function search() 
    {
        Session::get()->admin['search'][get_class($this)] = Request::get()->raw->post['Search'];
        redirect('/offers');
    }
   
    public function search_reset() 
    {
        Session::get()->admin['search'][get_class($this)] = array();
        redirect('/offers');
    }
   
    public function delete() 
    {
        $model = $this->getModel(Request::get()->id);
        if (!$model->exists()) 
        {
            \Log::error("Element nie istnieje");
            redirect($this->url);
        }
        $model->delete();
        Log::confirm("Element został skasowany");
        redirect($this->url);
    }
    
    //==========================================================================
    // general tab
    
    protected function extendGeneralViewData(& $viewData)
    {
        $city = new City();
        $list = $city->list_all();
        $viewData['cities'] = $list->data['elements'];
        
        $cats = new Category();
        $list = $cats->list_all();
        $viewData['categories'] = $list->data['elements'];
    }
   
    public function edit() 
    {
        $model = $this->getModel(Request::get()->id);
        if (!$model->exists()) {
            \Log::error("Element nie istnieje");
            redirect($this->url);
        }
      
        if (isset(Request::get()->raw->post['Edit'])) {
            if ($model->update(Request::get()->raw->post['Edit'])) {
                Log::confirm("Zmiany zostały zapisane");
                if (!empty($_GET['redirect'])) {
                    redirect($_GET['redirect']);
                }
                redirect($this->url.'/edit/id:'.$model->id);
            }
        }
      
        $model->getData(true);
      
        $viewData = array(
            'model'=>$model,
            'host' => "http://".$_SERVER['HTTP_HOST'],
        );
        $this->extendGeneralViewData($viewData);
        $view = new webView($viewData, 'offers/general.htm');
      
        return $view->show();
    }
   
    public function add() 
    {  
        $model = $this->getModel();
        
        if (isset(Request::get()->raw->post['Edit'])) {
            $data = Request::get()->raw->post['Edit'];
            $data['user_id'] = Admin::dajId();
            if ($model->add($data)) {
                Log::confirm("Element został utworzony");
                redirect($this->url.'/edit/id:'.$model->id);
            }
        }
        
        $model->getData(true);
        $viewData = array(
            'model'=>$model,
        );
        $this->extendGeneralViewData($viewData);
        $view = new webView($viewData, 'offers/general.htm');
      
        return $view->show();
    }
    
    //==========================================================================
    //edycja wariantu
   
    public function editVariant() 
    {
        $model = $this->getModel(Request::get()->id);
        $variant = Request::get()->variant;
        if (!$model->exists() || empty($variant)) {
            \Log::error("Element nie istnieje");
            redirect($this->url);
        }
      
        if (isset(Request::get()->raw->post['Edit'])) {
            if ($model->saveVariant($variant, Request::get()->raw->post['Edit'])) {
                Log::confirm("Zmiany zostały zapisane");
                if (!empty($_GET['redirect'])) {
                    redirect($_GET['redirect']);
                }
                redirect($this->url.'/editVariant/id:'.$model->id.'/variant:'.$variant);
            }
        }
      
        $model->getData(true);
      
        $category = new Category($model->data['category_id']);
        $category->getData(true);
        $viewData = array(
            'model' => $model,
            'variant' => Request::get()->variant,
            'category' => $category,
        );
        
        $view = new webView($viewData, 'offers/variant.htm');
      
        return $view->show();
    }
    
    public function copyOptions()
    {
        $model = $this->getModel(Request::get()->id);
        $variant = Request::get()->variant;
        if (!$model->exists() || empty($variant)) {
            \Log::error("Element nie istnieje");
            redirect($this->url);
        }
        
        $model->copyVariant(1, $variant);
        Log::confirm("Opcje zostały skopiowane");
        redirect($this->url.'/editVariant/id:'.$model->id.'/variant:'.$variant);
    }
    
    //==========================================================================
    // insturments section
    
    public function instruments()
    {
        $model = $this->getModel(Request::get()->id);
        
        if (!$model->exists()) {
            \Log::error("Element nie istnieje");
            redirect($this->url);
        }
        
        if (isset(Request::get()->raw->post['Edit'])) {
            if ($model->saveInstruments(Request::get()->raw->post['Edit'])) {
                Log::confirm("Zmiany zostały zapisane");
                if (!empty($_GET['redirect'])) {
                    redirect($_GET['redirect']);
                }
                redirect($this->url.'/instruments/id:'.$model->id);
            }
        }
      
        $model->getData(true);
      
        $instruments = new Instrument();
        $instrumentsList = $instruments->list_all();
        
        $viewData = array(
            'model' => $model,
            'instruments' => $instrumentsList->data['elements'],
        );
        
        $view = new webView($viewData, 'offers/instruments.htm');
      
        return $view->show();
    }
    
    public function summary()
    {
        $model = $this->getModel(Request::get()->id);
        
        if (!$model->exists()) {
            \Log::error("Element nie istnieje");
            redirect($this->url);
        }
        
        if (isset(Request::get()->raw->post['SummarySettings'])) {
            if ($model->saveSummarySettings(Request::get()->raw->post['SummarySettings'])) {
                Log::confirm("Zmiany zostały zapisane");
                if (!empty($_GET['redirect'])) {
                    redirect($_GET['redirect']);
                }
                redirect($this->url.'/summary/id:'.$model->id);
            }
        }
        
        $model->getData(true);
        $model->prepareSummary();
      
        $instruments = new Instrument();
        $instrumentsList = $instruments->list_all();
        
        $viewData = array(
            'model' => $model,
            'instruments' => $instrumentsList->data['elements'],
            'pdfLink' => '/oferta/'.$model->data['hash'].'/pdf',
        );
        
        $view = new webView($viewData, 'offers/summary.htm');
      
        return $view->show();
    }
}
