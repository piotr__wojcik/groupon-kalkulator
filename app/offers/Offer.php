<?php

namespace app\offers;

use \BazaDanych;
use \Log;
use \app\settings\Categories\Category;
use \app\settings\Cities\City;
use \app\settings\Instruments\Instrument;
use \app\admin\admin_user;

class Offer extends \Model 
{
    const NICKEL_COMMISSION_COLOR = "nickel";
    const BRONZE_COMMISSION_COLOR = "bronze";
    const SILVER_COMMISSION_COLOR = "silver";
    const GOLD_COMMISSION_COLOR = "gold";
    const PLATINUM_COMMISSION_COLOR = "platinum";

    public function __construct($id = NULL) 
    {
        $this->id = (int)$id;
        $this->baza = \BazaDanych::dajBaze();
        $this->tabela = "offers";
    }
    
    public function initWithHash($hash)
    {
        cleanData($hash);
        $check = fetch_assoc($this->baza->sql("
            select 
                {$this->tabela}.id
            from 
                {$this->tabela}
            where
                hash='{$hash}'
        "));
        if (!empty($check['id'])) {
            $this->id = (int)$check['id'];
        }
    }
   
    public function list_all($params = array(), $page = 1, $onPage = 1000000) 
    {
        cleanData($params);
        if (empty($params['order_by'])) {
            $params['order_by'] = "created_date";
        }
        if (empty($params['order_how'])) {
            $params['order_how'] = "desc";
        }
        
        $sql = "
        select 
            {$this->tabela}.id
        from 
            {$this->tabela}
        where
            1
            
            " . (!empty($params['user_id']) ?
                (is_array($params['user_id']) 
                    ? " and (0 or user_id='".implode("' or user_id='", $params['user_id'])."') "
                    : " and user_id='{$params['user_id']}'"
                )
            : "") . "
            
            " . (!empty($params['name']) ? "
            and name like '%{$params['name']}%'
            " : "") . "
            
            and deleted = 0
        order by 
            {$params['order_by']} {$params['order_how']}
        ";
      
        return new \Lista($this, $sql, $page, $onPage);
    }
    
    public function checkData(&$dane, $update = false) 
    {
        trimData($dane);
        $error = false;
      
        if (isset($dane['name']) && empty($dane['name'])) {
            Log::error("Należy podać nazwę");
            $error = true;
        }
        if (empty($dane['category_id'])) {
            Log::error("Należy określić kategorię");
            $error = true;
        }
        if (empty($dane['cities_ids'])) {
            Log::error("Należy wybrać co najmniej jedno miasto");
            $error = true;
        }
        
        switch ($dane['commission_color']) {
            case self::NICKEL_COMMISSION_COLOR:
            case self::BRONZE_COMMISSION_COLOR:
            case self::SILVER_COMMISSION_COLOR:
            case self::GOLD_COMMISSION_COLOR:
            case self::PLATINUM_COMMISSION_COLOR:
                break;
            default:
                $dane['commission_color'] = self::NICKEL_COMMISSION_COLOR;
        }
        
        $dane['no_of_variants'] = (int)$dane['no_of_variants'];
        $dane['no_of_variants'] = max(0, $dane['no_of_variants']);
        $dane['no_of_variants'] = min(10, $dane['no_of_variants']);
        
        if (!$error) {
            $dane['last_modified_date'] = date("Y-m-d H:i:s");
            $dane['cities_ids'] = serialize($dane['cities_ids']);
        }
        
        return !$error;
    }
    
    protected function afterRead() 
    {
        //kategoria
        $category = new Category($this->data['category_id']);
        $category->getData(true);
        $this->data['category'] = $category;
        //miasta
        $this->data['cities_ids'] = unserialize($this->data['cities_ids']);
        if (!is_array($this->data['cities_ids'])) {
            $this->data['cities_ids'] = array();
        }
        $this->data['cities'] = array();
        foreach ($this->data['cities_ids'] as $city_id) {
            $city = new City($city_id);
            $city->getData(true);
            $this->data['cities'][$city_id] = $city;
        }
        //warianty
        for ($i=1; $i<=10; $i++) {
            $field = "variant{$i}";
            $this->data[$field] = unserialize($this->data[$field]);
            if (!is_array($this->data[$field])) {
                $this->data[$field] = array();
            }
        }
        //domyslne instrumenty
        for ($i=1; $i<=10; $i++) {
            $field = "variant{$i}";
            if (!is_array($this->data[$field]['instruments'])) {
                $this->data[$field]['instruments'] = array(
                    1 => array('use' => 1),
                    6 => array('use' => 1),
                    7 => array('use' => 1),
                    12 => array('cities'=>array()),
                    13 => array('use' => 1),
                );
                //dla mailingu troche inaczej (miasta)
                foreach ($this->data['cities_ids'] as $city_id) {
                    $this->data[$field]['instruments'][12]['cities'][$city_id]['use'] = 1;
                }
            }
        }
        //domyslna prowizja
        if (
            !empty($this->data['commission_color']) &&
            !empty($this->data['category'])
        ) {
            $defaultCommission = null;
            $this->data['defaultCommission'] = $this->data['category']->data[$this->data['commission_color'].'_minimal_commission'];
        }
        
        //wlasciciel
        $user = new admin_user($this->data['user_id']);
        $user->getData(true);
        $this->data['owner'] = $user;
        
        $this->data['summary_settings'] = unserialize($this->data['summary_settings']);
        if (!is_array($this->data['summary_settings'])) {
            $this->data['summary_settings'] = array(
                'showOptions' => 1,
                'normalPrice' => 1,
                'price' => 1,
                'discount' => 0,
                'commission' => 0,
                'partnerGain' => 1,
                'grouponsCount' => 1,
                'sumPartnerGain' => 1,
                'valueForCoupon' => 1,
            );
        }
    }
   
    protected function beforeAdd(&$dane) 
    {
        $dane['created_date'] = date("Y-m-d H:i:s");
        do {
            $dane['hash'] = md5(time().rand(0,100).rand(0,100).rand(0,100).rand(0,100));
            $check = fetch_assoc($this->baza->sql("
                select
                    id
                from
                    {$this->tabela}
                where
                    hash='{$dane[hash]}'
            "));
        } while (!empty($check['id']));
    }
   
    public function delete()
    {
        $update = array(
            'deleted' => 1,
            'deleted_date' => date("Y-m-d H:i:s"),
        );
        $this->doUpdate($update);
    }
    
    public function saveVariant($variant_no, $dane)
    {
        $this->getData(true);
        $field = "variant{$variant_no}";
        
        $dataToSave = $dane;
        $dataToSave['instruments'] = $this->data[$field]['instruments'];
        $updateData = array(
            $field => serialize($dataToSave),
            'last_modified_date' => date("Y-m-d H:i:s"),
        );
        $this->updateAfterCheck($updateData);
        return true;
    }
    
    public function copyVariant($whichToCopy, $copyTo) 
    {
        $this->getData(true);
        $fieldFrom = "variant{$whichToCopy}";
        $fieldTo = "variant{$copyTo}";
        $updateData = array(
            $fieldTo => serialize($this->data[$fieldFrom]),
            'last_modified_date' => date("Y-m-d H:i:s"),
        );
        $this->updateAfterCheck($updateData);
        return true;
    }
   
    public function saveInstruments($data) 
    {
        $this->getData(true);
        $updateData = array(
            'last_modified_date' => date("Y-m-d H:i:s"),
        );
        if (!is_array($data)) {
            $data = array();
        }
        foreach ($data as $variantId => $variantData) {
            $fieldId = "variant{$variantId}";
            $tmp[$fieldId] = $this->data[$fieldId];
            $tmp[$fieldId]['instruments'] = $variantData['instruments'];
            $updateData[$fieldId] = serialize($tmp[$fieldId]);
        }
        
        $this->updateAfterCheck($updateData);
        return true;
    }
    
    public function saveSummarySettings($data)
    {
        $this->getData(true);
        $updateData = array(
            'last_modified_date' => date("Y-m-d H:i:s"),
            'summary_settings' => serialize($data['viewOptions']),
            'user_comment' => $data['user_comment'],
            'comment' => $data['comment'],
        );
        $this->updateAfterCheck($updateData);
        return true;
    }
    
    public function prepareSummary()
    {
        $this->getData(true);
        $instruments = new Instrument();
        $instrumentsListTmp = $instruments->list_all();
        $instrumentsList = array();
        $usedInstrumentsList = array();
        foreach ($instrumentsListTmp->data['elements'] as $elt) {
            $instrumentsList[$elt->id] = $elt;
        }
        
        //liczymy
        for ($i = 1; $i<=$this->data['no_of_variants']; $i++) {
            $totalSum = 0;
            //przelew
            $fieldId = 'variant'.$i;
            $totalSum += $this->data[$fieldId]['summary']['partnerGain'];
            $this->data[$fieldId]['summary']['partnerGain'] = $this->formatValue(
                $this->data[$fieldId]['summary']['partnerGain']
            );
            
            //instrumenty
            $instrumentsRange = 0;
            $instrumentsValue = 0;
            $instrumentNames = array();
            
            if (is_array($this->data[$fieldId]['instruments'])) {
                foreach ($this->data[$fieldId]['instruments'] as $instrId=>$instrData) {
                    if (isset($instrumentsList[$instrId])) {
                        if ($instrData['use']) {
                            $usedInstrumentsList[$instrId] = $instrumentsList[$instrId];
                        }
                        
                        $instrValues = $instrumentsList[$instrId]->getRangeNameAndValue(
                            $instrData['use'],
                            $instrData['cities'],
                            $instrData['duration'],
                            $instrData['numberOf']
                        );
                        if ($instrValues !== false) {
                            $instrumentsRange += $instrValues['range'];
                            $instrumentsValue += $instrValues['value'];
                            $totalSum += $instrValues['value'];
                            $instrumentNames[] = $instrValues['name'];
                        }
                    }
                    
                }
            }
            
            $this->data[$fieldId]['summary']['instrumentsRange'] = $this->formatValue(
                $instrumentsRange
            );
            $this->data[$fieldId]['summary']['instrumentsValue'] = $this->formatValue(
                $instrumentsValue
            );
            $this->data[$fieldId]['summary']['instrumentNames'] = $instrumentNames;
            
            //returngin value
            $returningValue = $this->data[$fieldId]['summary']['grouponsCount']
                *($this->data[$fieldId]['summary']['returningPercent']/100)
                *$this->data[$fieldId]['summary']['returningGain'];        
            $totalSum += $returningValue;
            $this->data[$fieldId]['summary']['returningValue'] = $this->formatValue(
                $returningValue                 
            );
            
            $this->data[$fieldId]['summary']['totalSum'] = $totalSum;
            $this->data['usedInstruments'] = $usedInstrumentsList;
        }
    }
    
    protected function formatValue($value)
    {
        if ((int)$value == $value) {
            return number_format((float)$value, 0, ',', ' ');;
        } else {
            return number_format((float)$value, 2, ',', ' ');
        }
    }
}
