<?php

namespace app\settings\Cities;

use \BazaDanych;
use \Log;

class City extends \Model 
{

    public function __construct($id = NULL) 
    {
        $this->id = (int)$id;
        $this->baza = \BazaDanych::dajBaze();
        $this->tabela = "cities";
    }
   
    public function list_all($params = array(), $page = 1, $onPage = 1000000) 
    {
        if (empty($params['order_by'])) {
            $params['order_by'] = "name";
        }
        if (empty($params['order_how'])) {
            $params['order_how'] = "asc";
        }
        
        $sql = "
        select 
            {$this->tabela}.id
        from 
            {$this->tabela}
        where
            deleted = 0
        order by 
            {$params['order_by']} {$params['order_how']}
        ";
      
        return new \Lista($this, $sql, $page, $onPage);
    }
    
    public function checkData(&$dane, $update = false) 
    {
        trimData($dane);
        $error = false;
      
        if (isset($dane['name']) && empty($dane['name'])) {
            Log::error("Należy podać nazwę");
            $error = true;
        }
        
        $dane['last_modified_date'] = date("Y-m-d H:i:s");
        
        return !$error;
    }
   
    protected function afterRead() 
    {

    }
   
    protected function beforeAdd(&$dane) 
    {
        $dane['created_date'] = date("Y-m-d H:i:s");
    
    }
   
    public function delete()
    {
        $update = array(
            'deleted' => 1,
            'deleted_date' => date("Y-m-d H:i:s"),
        );
        $this->doUpdate($update);
    }
   
}
