<?php

use \app\settings\Cities\City;

class CitiesController extends SettingsController 
{
    
    protected $listTemplate = "settings/cities/list.htm";
    protected $crudTemplate = "settings/cities/change.htm";
    protected $url = "/cities";
   
    public function getModel($id = null)
    {
        return new City($id);
    } 
   
    protected function extendViewData(& $viewData)
    {
    
    }
   
}
