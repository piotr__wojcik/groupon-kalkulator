<?php

use \app\core\webView;
use \app\settings\Instruments\Instrument;

class FaqController extends adminController 
{
    
    protected $template = 'static/faq.html';
    
    public function show() 
    {
        $instruments = new Instrument();
        $instrumentsList = $instruments->list_all();

        $viewData = array(
            'instruments' => $instrumentsList->data['elements'],
        );
        $view = new webView($viewData, $this->template);
      
        return $view->show();
    }
    
}
