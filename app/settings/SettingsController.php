<?php

use \app\core\webView;

class SettingsController extends adminController 
{

    const PRV_EDIT = "settings_edit";
    
    protected $listTemplate = "";
    protected $crudTemplate = "";
    protected $url = "";
   
    public function beforeExecute() 
    {
        parent::beforeExecute();
        if (!Admin::czyMoze(self::PRV_EDIT)) {
            redirect('/admin');
        }
    }
   
    public function getModel($id = null)
    {
        return new Model($id);
    } 
   
    public function show() 
    {
        $model = $this->getModel();
        
        $list = $model->list_all(
            Session::get()->admin['search'][get_class($this)], 
            \Request::get()->get['pageNr'], 
            10000
        );
      
        $viewData = array(
            'list'=>$list,
            'search'=>Session::get()->admin['search'][get_class($this)],
            'page' => \Request::get()->get['pageNr'],
            'pages' => paginator($list->data['page'], $list->data['pages'], $this->url),
        );
        $view = new webView($viewData, $this->listTemplate);
      
        return $view->show();
    }
   
    public function search() 
    {
        Session::get()->admin['search'][get_class($this)] = Request::get()->raw->post['Search'];
        redirect('/clients');
    }
   
    public function search_reset() 
    {
        Session::get()->admin['search'][get_class($this)] = array();
        redirect('/clients');
    }
   
    public function delete() 
    {
        $model = $this->getModel(Request::get()->id);
        if (!$model->exists()) 
        {
            \Log::error("Element nie istnieje");
            redirect($this->url);
        }
        $model->delete();
        Log::confirm("Element został skasowany");
        redirect($this->url);
    }
    
    protected function extendViewData(& $viewData)
    {
    
    }
   
    public function edit() 
    {
        $model = $this->getModel(Request::get()->id);
        if (!$model->exists()) {
            \Log::error("Element nie istnieje");
            redirect($this->url);
        }
      
        if (isset(Request::get()->raw->post['Edit'])) {
            if ($model->update(Request::get()->raw->post['Edit'])) {
                Log::confirm("Zmiany zostały zapisane");
                redirect($this->url.'/edit/id:'.$model->id);
            }
        }
      
        $model->getData(true);
      
        $viewData = array(
            'model'=>$model,
        );
        $this->extendViewData($viewData);
        $view = new webView($viewData, $this->crudTemplate);
      
        return $view->show();
    }
   
    public function add() 
    {  
        $model = $this->getModel();
        
        if (isset(Request::get()->raw->post['Edit'])) {
            if ($model->add(Request::get()->raw->post['Edit'])) {
                Log::confirm("Element został utworzony");
                redirect($this->url.'/edit/id:'.$model->id);
            }
        }
        
        $model->getData(true);
        $viewData = array(
            'model'=>$model,
        );
        $this->extendViewData($viewData);
        $view = new webView($viewData, $this->crudTemplate);
      
        return $view->show();
    }
   
}
