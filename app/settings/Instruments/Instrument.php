<?php

namespace app\settings\Instruments;

use \BazaDanych;
use \Log;
use \app\settings\cities\City;

class Instrument extends \Model 
{

    const TYPE_STANDARD = "standard";
    const TYPE_VARIANTS = "variants";
    const TYPE_CITIES = "cities";
    
    const DURATION_ONE_TIME = "none";
    const DURATION_IN_DAYS = "in-days";
    const DURATION_IN_WEEKS = "in-weeks";

    public function __construct($id = NULL) 
    {
        $this->id = (int)$id;
        $this->baza = \BazaDanych::dajBaze();
        $this->tabela = "instruments";
    }
   
    public function list_all($params = array(), $page = 1, $onPage = 1000000) 
    {
        if (empty($params['order_by'])) {
            $params['order_by'] = "id";
        }
        if (empty($params['order_how'])) {
            $params['order_how'] = "asc";
        }
        
        $sql = "
        select 
            {$this->tabela}.id
        from 
            {$this->tabela}
        where
            deleted = 0
        order by 
            {$params['order_by']} {$params['order_how']}
        ";
      
        return new \Lista($this, $sql, $page, $onPage);
    }
    
    public function checkData(&$dane, $update = false) 
    {
        trimData($dane);
        $error = false;
      
        if (isset($dane['name']) && empty($dane['name'])) {
            $dane['name'] = "Instrument";
        }
        
        $dane['last_modified_date'] = date("Y-m-d H:i:s");
        switch ($dane['type']) {
            case self::TYPE_STANDARD:
                $test = (int)$dane['data']['range'];
                if ($test>0) {
                    $dane['data']['range'] = (int) $dane['data']['range'];
                }
                if ($test<0) {
                    $dane['data']['range'] = "";
                }
                makeDecimal($dane['data']['price'],0);
                if ($dane['data']['price']<0) {
                    $dane['data']['price'] = 0;
                }
                unset($dane['data']['variants']);
                unset($dane['data']['cities']);
                unset($dane['data']['canSelectNumberOf']);
                break;
                
            case self::TYPE_VARIANTS:
                foreach ($dane['data']['variants'] as $i => & $variant) {
                    //to jakos ladniej dobrze by bylo zrobic
                    if ($i==="cloneTemplate") {
                        unset($dane['data']['variants'][$i]);
                        continue;
                    }
                    makeDecimal($variant['price'],0);
                    if ($variant['price']<0) {
                        $variant['price'] = 0;
                    }
                    $test = (int)$variant['range'];
                    if ($test>0) {
                        $variant['range'] = (int) $variant['range'];
                    }
                    if ($test<0) {
                        $variant['range'] = "";
                    }
                }
                usort($dane['data']['variants'], array($this, 'sortVariants'));
                
                unset($dane['data']['price']);
                unset($dane['data']['range']);
                unset($dane['data']['cities']);
                break;
                
            case self::TYPE_CITIES:
                foreach ($dane['data']['cities'] as $i => & $variant) {
                    if (empty($variant['price']) && empty($variant['range'])) {
                        $variant['price'] = $variant['range'] = "";
                        continue;
                    }
                    makeDecimal($variant['price'],0);
                    if ($variant['price']<0) {
                        $variant['price'] = 0;
                    }
                    $test = (int)$variant['range'];
                    if ($test>0) {
                        $variant['range'] = (int) $variant['range'];
                    }
                    if ($test<0) {
                        $variant['range'] = "";
                    }
                }

                unset($dane['data']['price']);
                unset($dane['data']['range']);
                unset($dane['data']['variants']);
                break;
            default:
                $dane['type'] = self::TYPE_STANDARD;
        }
        
        switch ($dane['duration_type']) {
            case self::DURATION_ONE_TIME:
            case self::DURATION_IN_DAYS:
            case self::DURATION_IN_WEEKS:
            
                break;
            default:
                $dane['duration_type'] = self::DURATION_ONE_TIME;
        } 
        
        $dane['data'] = serialize($dane['data']);
    
        
        return !$error;
    }
    
    protected function sortVariants($a, $b)
    {
        return ($a['price'] < $b['price']) ? -1 : 1;
    }
   
    protected function afterRead() 
    {
        $this->data['data'] = unserialize($this->data['data']);
        if (!is_array($this->data['data'])) {
            $this->data['data'] = array();
        }   
        
        $this->data['normalized'] = textId($this->data['name']);
    }
    
    public function hasFAQ()
    {
        $this->getData(true);
        return !empty($this->data['faq']);
    }
   
    protected function beforeAdd(&$dane) 
    {
        $dane['created_date'] = date("Y-m-d H:i:s");
        
    }
   
    public function delete()
    {
        $update = array(
            'deleted' => 1,
            'deleted_date' => date("Y-m-d H:i:s"),
        );
        $this->doUpdate($update);
    }
   
    public function getRangeNameAndValue($usedVariant, $usedCities, $duration, $numberOf = 0)
    {
        $this->getData(true);
        if (!$this->exists()) {
            return false;
        }
        $range = 0;
        $value = 0;
        $name = "";
        
        if (
            $this->data['type'] == self::TYPE_VARIANTS ||
            $this->data['type'] == self::TYPE_STANDARD
        ) {
        //======================================================================
        //standard i warianty
        
            if ($this->data['type'] == self::TYPE_VARIANTS) {
                if (isset($this->data['data']['variants'][$usedVariant])) {
                    $name = $this->data['name'] . " - " . $this->data['data']['variants'][$usedVariant]['name'];
                    $value += (float)$this->data['data']['variants'][$usedVariant]['price'];
                    $range += (int)$this->data['data']['variants'][$usedVariant]['range']; 
                }
                
            } elseif ($usedVariant==1 || (int)$duration>0) {
                $name = $this->data['name'];
                $value += (float)$this->data['data']['price'];
                $range += (int)$this->data['data']['range']; 
            }
            
            if ($this->data['data']['canSelectNumberOf']==1) {
                $value *= (int)$numberOf;
                $range *= (int)$numberOf;
            }
            
            if (
                (
                    $this->data['duration_type'] == self::DURATION_IN_DAYS ||
                    $this->data['duration_type'] == self::DURATION_IN_WEEKS
                ) &&
                (int) $duration>0
            ) {
                $value *= (int)$duration;
                $range *= (int)$duration;
                
                if ($this->data['duration_type'] == self::DURATION_IN_DAYS) {
                    $name .= " - dni: ".(int)$duration;
                }
                if ($this->data['duration_type'] == self::DURATION_IN_WEEKS) {
                    $name .= " - tygodni: ".(int)$duration;
                }
            }
        } else {
        //======================================================================
        //miasta
            if (is_array($usedCities)) {
                $cosjest = false;
                $cities_names = array();
                foreach ($usedCities as $city_id => $city_data) {
                    if (
                        (int)$city_data['use']==0 &&
                        (int)$city_data['duration']==0 &&
                        (int)$city_data['numberOf']==0
                    ) {
                        continue;
                    } else {
                        $cosjest = true;
                    }
                    
                    
                    if (isset($this->data['data']['cities'][$city_id])) {
                        $cosjest = true;
                        
                        $city = new City($city_id);
                        $city->getData(true);
                        $city_name = $city->data['name'];
                        $city_region = $city->data['region'];
                        $city_value = $this->data['data']['cities'][$city_id]['price'];
                        $city_range = $this->data['data']['cities'][$city_id]['range'];
                        
                        if ($this->data['data']['canSelectNumberOf']==1) {
                            $city_value *= (int)$city_data['numberOf'];
                            $city_range *= (int)$city_data['numberOf'];
                            $city_name .= " x".(int)$city_data['numberOf'];
                        }
                        
                        if (
                            (
                                $this->data['duration_type'] == self::DURATION_IN_DAYS ||
                                $this->data['duration_type'] == self::DURATION_IN_WEEKS
                            )
                        ) {
                            $city_value *= (int)$city_data['duration'];
                            $city_range *= (int)$city_data['duration'];
                            
                            if ($this->data['duration_type'] == self::DURATION_IN_DAYS) {
                                $city_name .= " - dni: ".(int)$city_data['duration'];
                            }
                            if ($this->data['duration_type'] == self::DURATION_IN_WEEKS) {
                                $city_name .= " - tygodni: ".(int)$city_data['duration'];
                            }
                        }
                        
                        $value += $city_value;
                        $range += $city_range;
                         
                        //newsweek store regions, not cities
                        if ($this->id == 4) {
                            if (!in_array($city_region, $cities_names)) {
                                $cities_names[] = $city_region;
                            }
                        } else {
                            $cities_names[] = $city_name;
                        }
                    }
                    
                }
                if ($cosjest) {
                    $name = $this->data['name']." (".implode("; ", $cities_names).")";
                }
            }
        
        }
        
        //echo "{$name}: {$value} | $range u.u.\n";
        
        //======================================================================
        //koniec
        
        if (empty($name) && empty($value) && empty($range)) {
            return false;
        }
        
        return array(
            'name' => $name,
            'value' => $value,
            'range' => $range,
        );
    }
}
