<?php

use \app\settings\Instruments\Instrument;
use \app\settings\Cities\City;

class InstrumentsController extends SettingsController 
{
    
    protected $listTemplate = "settings/instruments/list.htm";
    protected $crudTemplate = "settings/instruments/change.htm";
    protected $url = "/instruments";
   
    public function getModel($id = null)
    {
        return new Instrument($id);
    } 
   
    protected function extendViewData(& $viewData)
    {
        $city = new City();
        $list = $city->list_all();
        $viewData['cities'] = $list->data['elements'];
    }
   
}
