<?php

namespace app\settings\Categories;

use \BazaDanych;
use \Log;

class Category extends \Model 
{

    public function __construct($id = NULL) 
    {
        $this->id = (int)$id;
        $this->baza = \BazaDanych::dajBaze();
        $this->tabela = "categories";
    }
   
    public function list_all($params = array(), $page = 1, $onPage = 1000000) 
    {
        if (empty($params['order_by'])) {
            $params['order_by'] = "name";
        }
        if (empty($params['order_how'])) {
            $params['order_how'] = "asc";
        }
        
        $sql = "
        select 
            {$this->tabela}.id
        from 
            {$this->tabela}
        where
            deleted = 0
        order by 
            {$params['order_by']} {$params['order_how']}
        ";
      
        return new \Lista($this, $sql, $page, $onPage);
    }
    
    public function checkData(&$dane, $update = false) 
    {
        trimData($dane);
        $error = false;
      
        if (isset($dane['name']) && empty($dane['name'])) {
            Log::error("Należy podać nazwę");
            $error = true;
        }
        
        if (!$error) {
            $dane['last_modified_date'] = date("Y-m-d H:i:s");
            
            makeDecimal($dane['nickel_minimal_commission'], 2);
            makeDecimal($dane['bronze_minimal_commission'], 2);
            makeDecimal($dane['silver_minimal_commission'], 2);
            makeDecimal($dane['gold_minimal_commission'], 2);
            makeDecimal($dane['platinum_minimal_commission'], 2);
            
            foreach ($dane['warnings'] as $i => & $warning) {
                //to jakos ladniej dobrze by bylo zrobic
                if ($i==="cloneTemplate") {
                    unset($dane['warnings'][$i]);
                    continue;
                }
                makeDecimal($warning['from'],2);
                if ($warning['from']>100) {
                    $warning['from'] = 100;
                }
                makeDecimal($warning['to'],2);
                if ($warning['to']<0) {
                    $warning['to'] = 0;
                }
                if ($warning['to'] > $warning['from']) {
                    $warning['to'] = $warning['from'];
                }
            }
            usort($dane['warnings'], array($this, 'sortWarnings'));
            $dane['warnings'] = serialize($dane['warnings']);
        }
        
        return !$error;
    }
    
    protected function sortWarnings($a, $b)
    {
        return ($a['from'] > $b['from']) ? -1 : 1;
    }
   
    protected function afterRead() 
    {
        $this->data['warnings'] = unserialize($this->data['warnings']);
        if (!is_array($this->data['warnings'])) {
            $this->data['warnings'] = array();
        }   
    }
   
    protected function beforeAdd(&$dane) 
    {
        $dane['created_date'] = date("Y-m-d H:i:s");
        
    }
   
    public function delete()
    {
        $update = array(
            'deleted' => 1,
            'deleted_date' => date("Y-m-d H:i:s"),
        );
        $this->doUpdate($update);
    }
   
}
