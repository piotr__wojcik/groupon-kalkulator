<?php

use \app\settings\Categories\Category;

class CategoriesController extends SettingsController 
{
    
    protected $listTemplate = "settings/categories/list.htm";
    protected $crudTemplate = "settings/categories/change.htm";
    protected $url = "/categories";
   
    public function getModel($id = null)
    {
        return new Category($id);
    } 
   
    protected function extendViewData(& $viewData)
    {
    
    }
   
}
