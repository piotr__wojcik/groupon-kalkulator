<?php

function format_value($value) {
    if ((int)$value == $value) {
        return number_format((float)$value, 0, ',', ' ');;
    } else {
        return number_format((float)$value, 2, ',', ' ');
    }
}

function parserDaty3($data) {
      $miesiace = array("sty","lut","mar","kwi","maj","cze","lip","sie","wrz","paź","lis","gru");
      $pom = explode(" ",$data);
      $data = explode("-",$pom[0]);
      if (substr($data[2],0,1) == "0")
         $data[2] = substr($data[2],1);
      return $data[2]." ".$miesiace[(int)$data[1]-1]." ".$data[0].", {$pom[1]}";
}
   
function paginator($page, $maxPages, $address, $side = "right") {
    $overflow = 4;
    $msg="
    <div class=\"paginator {$side}\">
    ";
    
    if ($side == "right") {
        if ($page + $overflow < $maxPages ) {
            $msg .= "<a href=\"{$address}?pageNr={$maxPages}\">{$maxPages}</a> ";
        }
        if ($page + $overflow < $maxPages-1) {
            $msg .= " <span>...</span> ";
        }  
        for ($i=($page+$overflow) ; $i>=($page-$overflow); $i--) {
            if ($i<1 || $i>$maxPages) {
                continue;
            }
            $msg .= "<a".($i == $page ? " class=\"active\"" : "")." href=\"{$address}?pageNr={$i}\">{$i}</a>";
        }
        if ($page - $overflow > 2) {
            $msg .=" <span>...</span> ";
        }
        if ($page - $overflow > 1) {
            $msg .= "<a href=\"{$address}?pageNr=1\">1</a> ";
        }
        $msg.="
            <span class=\"title\">
                Strona:
            </span> 
        ";
    } else {
        $msg.="
            <span class=\"title\">
                Strona:
            </span> 
        ";
        if ($page - $overflow > 1) {
            $msg .= "<a href=\"{$address}?pageNr=1\">1</a> ";
        }
        if ($page - $overflow > 2) {
            $msg .=" <span>...</span> ";
        }
        for ($i=($page-$overflow) ; $i<=($page+$overflow); $i++) {
            if ($i<1 || $i>$maxPages) {
                continue;
            }
            $msg .= "<a".($i == $page ? " class=\"active\"" : "")." href=\"{$address}?pageNr={$i}\">{$i}</a>";
        }
        if ($page + $overflow < $maxPages-1) {
            $msg .= " <span>...</span> ";
        }
        if ($page + $overflow < $maxPages ) {
            $msg .= "<a href=\"{$address}?pageNr={$maxPages}\">{$maxPages}</a> ";
        }
    }
    $msg.="
    </div>
    ";
      
    if (empty($maxPages) || $maxPages==1) {
        return "";
    }
    return $msg;
}
