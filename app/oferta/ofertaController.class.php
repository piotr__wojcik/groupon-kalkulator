<?php

use \app\core\webView;
use \app\offers\Offer;
use \app\settings\Instruments\Instrument;

class OfertaController extends baseController 
{

    protected $url = "/oferta";
   
    public function getModel()
    {
        $m = new Offer();
        $m->initWithHash(\Request::get()->param1);
        $m->getData(true);
        return $m;
    } 
   
    public function show() 
    {
        return $this->summary();
    }
   
    public function summary()
    {
        if (\Request::get()->param2 == "pdf") {
            return $this->pdf();
        }
        if (\Request::get()->param2 == "pdfPreview") {
            return $this->pdfPreview();
        }
        
        $model = $this->getModel();
        
        if (!$model->exists()) {
            redirect('/');
        }
        
        $model->getData(true);
        $model->prepareSummary();
      
        $instruments = new Instrument();
        $instrumentsList = $instruments->list_all();
        
        $viewData = array(
            'model' => $model,
            'instruments' => $instrumentsList->data['elements'],
            'pdfLink' => '/oferta/'.\Request::get()->param1.'/pdf',
        );
        
        $view = new webView($viewData, 'summary.htm', \Config::get()->USER_TEMPLATES_DIR);
      
        return $view->show();
    }
    
    protected function pdf() {
        $model = $this->getModel();
        
        if (!$model->exists()) {
            redirect('/');
        }
        
        ini_set('memory_limit', '2000M');
        ini_set('max_execution_time', '60');
        
        $model->getData(true);
        $model->prepareSummary();
      
        $instruments = new Instrument();
        $instrumentsList = $instruments->list_all();
        
        $data = array(
            'model' => $model,
            'instruments' => $instrumentsList->data['elements'],
        );
        
        $pdf = new offerPDF();
        
        header("Content-type:application/pdf");
        //header("Content-Disposition:inline;filename=Groupon-" . textId($model->data['name']) . ".pdf");
        header("Content-Disposition:attachment;filename=Groupon-" . textId($model->data['name']) . ".pdf");
        echo $pdf->generate($data);
    }
    
    protected function pdfPreview() {
        $model = $this->getModel();
        
        if (!$model->exists()) {
            redirect('/');
        }
        
        ini_set('memory_limit', '2000M');
        ini_set('max_execution_time', '60');
        
        $model->getData(true);
        $model->prepareSummary();
      
        $instruments = new Instrument();
        $instrumentsList = $instruments->list_all();
        
        $data = array(
            'model' => $model,
            'instruments' => $instrumentsList->data['elements'],
        );
        
        $pdf = new offerPDF();
        
        header("Content-type:application/pdf");
        header("Content-Disposition:inline;filename=Groupon-" . textId($model->data['name']) . ".pdf");
        //header("Content-Disposition:attachment;filename=Groupon-" . textId($model->data['name']) . ".pdf");
        echo $pdf->generate($data);
    }
}
