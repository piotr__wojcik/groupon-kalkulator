<?php

use \app\core\webView;
use \app\editablePages\Page;
//use \Admin;
//use \SettingsController;

class editablePageController extends adminController 
{
    
    protected $template = 'static/template.html';
    protected $crudTemplate = 'static/edit/template.html';
    
    public function show() 
    {
        $model = $this->getModel();

        $viewData = array(
            'title' => $model->getTitle(),
            'content' => $model->getContent(),
            'canEdit' => $this->currentUserCanEdit(),
            'editLink' => $this->getEditLink(),
        );
        $this->extendViewData($viewData);
        $view = new webView($viewData, $this->template);
      
        return $view->show();
    }
    
    protected function extendViewData(& $viewData)
    {
    
    }
   
    //==========================================================================
    // edit functions
    
    public function getId()
    {
        //name of the class minus the length of 'Controller'
        return substr(get_class($this), 0, -10);
    }
    
    public function getModel()
    {
        return new Page($this->getId());
    }
    
    public function getLink()
    {
        //id is the link
        return "/".$this->getId();
    }
    
    public function getEditLink()
    {
        return $this->getLink()."/edit";
    }
    
    public function currentUserCanEdit()
    {
        return Admin::czyMoze(SettingsController::PRV_EDIT);
    }
    
    public function edit()
    {
        if (!$this->currentUserCanEdit()) {
            redirect('/admin');
        }
        
        $model = $this->getModel();
        
        if (isset(Request::get()->raw->post['Edit'])) {
            if ($model->update(Request::get()->raw->post['Edit'])) {
                Log::confirm("Zmiany zostały zapisane");
                redirect($this->getEditLink());
            }
        }
      
        $model->getData(true);
      
        $viewData = array(
            'model' => $model,
            'viewLink' => $this->getLink()
        );
        $view = new webView($viewData, $this->crudTemplate);
      
        return $view->show();
    }
}
