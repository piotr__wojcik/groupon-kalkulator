<?php

namespace app\editablePages;

use \BazaDanych;
use \Log;

class Page extends \Model 
{

    public function __construct($id = NULL) 
    {
        $this->id = escape_string($id);
        $this->baza = \BazaDanych::dajBaze();
        $this->tabela = "editable_pages";
    }
   
    public function checkData(&$dane, $update = false) 
    {
        trimData($dane);
        $dane['last_modified'] = date("Y-m-d H:i:s");
        
        return true;
    }
    
    public function beforeUpdate(&$dane)
    {
        //if entry doesn't exists, create it
        $check = fetch_assoc($this->baza->sql("
                select
                    id
                from
                    {$this->tabela}
                where
                    id = '{$this->id}'
            "));
        if (empty($check['id'])) {
            $addData = array(
                'id' => $this->id,
            );
            $this->add($addData);
        }
    }
    
    public function getLinkName()
    {
        $this->getData(true);
        return $this->data['link_name'];
    }
    
    public function getTitle()
    {
        $this->getData(true);
        return $this->data['title'];
    }
    
    public function getContent()
    {
        $this->getData(true);
        return $this->data['content'];
    }
   
}
